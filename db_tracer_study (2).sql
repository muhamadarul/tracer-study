-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 17, 2021 at 02:45 AM
-- Server version: 5.7.24
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tracer_study`
--

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

DROP TABLE IF EXISTS `alumni`;
CREATE TABLE IF NOT EXISTS `alumni` (
  `id_alumni` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `tmp_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jk` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `alamat` text,
  `email` varchar(150) DEFAULT NULL,
  `tlp` varchar(13) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT 'default.jpg',
  `thn_lulus` year(4) DEFAULT NULL,
  `jurusan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_alumni`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `alumni`
--

INSERT INTO `alumni` (`id_alumni`, `nama`, `tmp_lahir`, `tgl_lahir`, `jk`, `agama`, `alamat`, `email`, `tlp`, `password`, `foto`, `thn_lulus`, `jurusan`) VALUES
(1, 'Dicky Wahyudi', 'Bekasi', '1999-08-11', 'Laki-laki', 'Islam', 'Tanah Baru', 'wahyudic724@gmail.com', '9128319827', '$2y$10$N2JBdnjWaF8USzXaAe0L..mhPTwL4GhqFrFTKxoPfL2gHTnHY7Quy', 'default.jpg', 2021, 1),
(2, 'Wahyudi Arul', 'Jakarta', '1999-08-11', 'Perempuan', 'Islam', 'Tanah Baru', 'dickywahyudi99@gmail.com', '9128319827', '$2y$10$A5sKh2OOaHkK0w5nRkXHi.A5884Xoyhj88LkgTxJdlIoBW0kIgisy', 'cf4c58fb64cd943f8aa254be9fc90a95.jpg', 2021, 1),
(18, 'Muhamad Arul Sendri', 'Bekasi', '2021-02-06', 'Laki-laki', 'Islam', 'Jati Rimun 2', 'muhamadarul907@gmail.com', '083242423', '$2y$10$gVKfB2FjiWPxnP4eLpGxaeZIzCSWiRUXCsf3fLA8PrAj3btb54812', '35ff2aa0e06f43b225da85c52cbf9808.jpg', 2021, 2),
(20, 'Aang1', 'Pebayuran', '2021-03-31', 'Laki-laki', 'Islam', 'pebayuran', 'aang.mic17164028@gmail.com', '0978676', '$2y$10$2kwir4qyZWixcoUeWIM9BuEXKDvaSl5QiHNWKdOdm9rzIq6hLEaoy', 'a32e7770e85c26191807b3b5a2ab3b99.png', 2021, 2),
(21, 'Resti', 'Bekasi', '2021-03-26', 'Perempuan', 'Islam', 'cifest', 'resti@mail.com', '0212132898', '$2y$10$3GdLixfKdP/Gcx142jDsX.9SSD1wPA07.uAZCZcEdwWAvFVX1vAOu', '49c2f70d21a884d53adf051439857912.jpg', 2028, 1),
(22, 'tes', 'Tes', '2000-08-01', 'Laki-laki', 'Islam', 'Tes', 'Tes', '8000', '$2y$10$Bfz3n6YCN8WUn4fwidtf7eVqBpC3ZhOWcB4fHx976o9/a6zf83fVK', 'default.jpg', 2021, 1),
(23, 'Imron Apriyanto', 'Cilacap', '1999-04-10', 'Laki-laki', 'Islam', 'Pasir Sari', 'imronapriyanto060@gmail.com', '089123131132', '$2y$10$BTRIZVlXSO2w1Yfgg81D1OzGVdKndUkBsK80GOWsB622rlfmBGQaC', 'aa502e59df6c67e258bea1b4f446f78d.jpg', 2021, 2),
(24, 'Imam Dwi Prasetyo', 'Yogyakarta', '2021-04-10', 'Laki-laki', 'Islam', 'Pasir Gombong', 'imamdwiprasetyo@gmail.com', '02342342123', '$2y$10$lTb4ST5M2cD3OsWjnvHVquwZNsl3Wt3bE5b/fGVPtSmZ8cq/RKHtm', 'c75b7d7609dd2c14e36403ce9623933a.png', 2021, 2),
(25, 'Nama Tes Sistem', 'Bekasi', '1997-08-11', 'Laki-laki', 'Islam', 'Cikarang', 'tes@gmail.com', '812736190', '$2y$10$76ptZ2zQfTeBmQmxXxc9auL.AxbCpSsasxYdLn7brfI1oXlmaJDMa', 'default.jpg', 2021, 2),
(26, 'ika', 'cilacap', '1999-03-03', 'perempuan', 'islam', 'cikarang', 'ika@Mail.com', '021973', 'tes123', 'tes', 2021, 1);

-- --------------------------------------------------------

--
-- Table structure for table `info_akademik`
--

DROP TABLE IF EXISTS `info_akademik`;
CREATE TABLE IF NOT EXISTS `info_akademik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` varchar(255) NOT NULL,
  `dipost` int(11) NOT NULL,
  `dibuat` timestamp NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info_akademik`
--

INSERT INTO `info_akademik` (`id`, `judul`, `deskripsi`, `foto`, `dipost`, `dibuat`, `status`) VALUES
(3, 'Laporan Tracer Study', '<p>    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><div><br></div>', '44bbe2d4f8731797cc72bf03ccf6dfd2.png', 1, '2021-04-05 09:16:09', 1),
(4, 'contoh333', '<p>    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.222</p><div><br></div>', '97094ebafd3884e4b6b270ac4a8b7664.jpeg', 1, '2021-04-06 09:09:14', 0);

-- --------------------------------------------------------

--
-- Table structure for table `info_karir`
--

DROP TABLE IF EXISTS `info_karir`;
CREATE TABLE IF NOT EXISTS `info_karir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` varchar(255) NOT NULL,
  `dipost` int(11) NOT NULL,
  `dibuat` timestamp NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info_karir`
--

INSERT INTO `info_karir` (`id`, `judul`, `deskripsi`, `foto`, `dipost`, `dibuat`, `status`) VALUES
(1, 'Info Karir', '<p><span style=\"font-family: \" times=\"\" new=\"\" roman\";=\"\" font-size:=\"\" medium;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br></span><br></p>', 'fc1ee14f7e3eaebf8599b5162aa51395.png', 1, '2021-04-05 09:27:19', 1),
(2, 'contoh', '<p>adasadsada</p>', '9cef06e0fd5dfa79eecc36088202473f.png', 1, '2021-04-05 09:26:19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `keahlian`
--

DROP TABLE IF EXISTS `keahlian`;
CREATE TABLE IF NOT EXISTS `keahlian` (
  `id_keahlian` int(11) NOT NULL AUTO_INCREMENT,
  `alumni` int(11) DEFAULT NULL,
  `nama_keahlian` varchar(50) DEFAULT NULL,
  `persentase` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_keahlian`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keahlian`
--

INSERT INTO `keahlian` (`id_keahlian`, `alumni`, `nama_keahlian`, `persentase`) VALUES
(1, 18, 'Pemrograman PHP', 90),
(2, 18, 'Pemrograman Javascript', 54);

-- --------------------------------------------------------

--
-- Table structure for table `m_jawaban`
--

DROP TABLE IF EXISTS `m_jawaban`;
CREATE TABLE IF NOT EXISTS `m_jawaban` (
  `id_jawaban` int(11) NOT NULL AUTO_INCREMENT,
  `kuesioner` int(11) NOT NULL,
  `jawaban` varchar(255) NOT NULL,
  PRIMARY KEY (`id_jawaban`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_jawaban`
--

INSERT INTO `m_jawaban` (`id_jawaban`, `kuesioner`, `jawaban`) VALUES
(1, 1, 'Bekerja'),
(2, 1, 'Tidak Bekerja'),
(3, 1, 'Belum Bekerja'),
(4, 1, 'Berwirausaha'),
(5, 1, 'Melanjutkan Studi'),
(6, 11, 'Ya'),
(7, 11, 'Tidak'),
(8, 3, 'Paruh Waktu'),
(9, 3, 'Penuh Waktu'),
(10, 2, 'Sesuai Bidang Ilmu'),
(11, 2, 'Diluar Bidang Ilmu'),
(12, 2, 'Tidak, Saya Berwirausaha'),
(13, 6, 'Melalui iklan di koran/majalah, brosur'),
(14, 6, 'Melamar ke perusahaan tanpa mengetahui lowongan yang ada'),
(15, 6, 'Pergi ke bursa/pameran kerja'),
(16, 6, 'Mencari lewat internet/iklan online/milis'),
(17, 6, 'Dihubungi oleh perusahaan'),
(18, 6, 'Menghubungi Kemenakertrans'),
(19, 6, 'Menghubungi agen tenaga kerja komersial/swasta'),
(20, 6, 'Memeroleh informasi dari pusat/kantor pengembangan karir fakultas/universitas'),
(21, 6, 'Menghubungi kantor kemahasiswaan/hubungan alumni'),
(22, 6, 'Membangun jejaring (network) sejak masih kuliah'),
(23, 6, 'Melalui relasi (misalnya dosen, orang tua, saudara, teman, dll.) '),
(24, 6, 'Membangun bisnis sendiri '),
(25, 6, 'Melalui penempatan kerja atau magang'),
(26, 6, 'Bekerja di tempat yang sama dengan tempat kerja semasa kuliah'),
(27, 7, 'Sangat Sesuai'),
(28, 7, 'Sesuai'),
(29, 7, 'Sedang'),
(30, 7, 'Tidak Sesuai'),
(31, 7, 'Sangat Tidak Sesuai'),
(32, 9, 'Instansi pemerintah (termasuk BUMN)'),
(33, 9, 'Organisasi non-profit/Lembaga Swadaya Masyarakat'),
(34, 9, 'Perusahaan swasta'),
(35, 9, 'Wiraswasta/perusahaan sendiri'),
(36, 9, 'Lainnya'),
(37, 8, 'Setingkat Lebih Tinggi'),
(38, 8, 'Tingkat yang Sama'),
(39, 8, 'Setingkat Lebih Rendah'),
(40, 8, 'Tidak Perlu Pendidikan Tinggi'),
(41, 10, 'Lebih dari 10 Juta'),
(42, 10, '9-10 Juta'),
(43, 10, '8-9 Juta'),
(44, 10, '7-8 Juta'),
(45, 10, '6-7 Juta'),
(46, 10, '5-6 Juta'),
(47, 10, '4-5 Juta'),
(48, 10, '3-4 Juta'),
(49, 10, '2-3 Juta'),
(50, 10, 'Kurang dari 2 Juta'),
(52, 5, 'Kurang dari Satu Bulan'),
(53, 5, '1-2 Bulan'),
(54, 5, '3-6 Bulan'),
(55, 5, '7-12 Bulan'),
(56, 5, '1-2 Tahun'),
(57, 5, 'Lebih dari 2 Tahun'),
(58, 12, 'Ini jawaban tes kuesioner');

-- --------------------------------------------------------

--
-- Table structure for table `m_jurusan`
--

DROP TABLE IF EXISTS `m_jurusan`;
CREATE TABLE IF NOT EXISTS `m_jurusan` (
  `id_jurusan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jurusan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_jurusan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_jurusan`
--

INSERT INTO `m_jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
(1, 'Sistem Informasi'),
(2, 'Teknik Informatika');

-- --------------------------------------------------------

--
-- Table structure for table `m_kuesioner`
--

DROP TABLE IF EXISTS `m_kuesioner`;
CREATE TABLE IF NOT EXISTS `m_kuesioner` (
  `id_kuesioner` int(11) NOT NULL AUTO_INCREMENT,
  `urutan` int(11) NOT NULL,
  `kuesioner` text NOT NULL,
  PRIMARY KEY (`id_kuesioner`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_kuesioner`
--

INSERT INTO `m_kuesioner` (`id_kuesioner`, `urutan`, `kuesioner`) VALUES
(1, 1, '<p>Apa aktivitas anda setelah lulus ?<br></p>'),
(2, 4, '<p>Apa pekerjaan anda sekarang sesuai dengan bidang ilmu yang anda dikuasai ?</p>'),
(3, 3, '<p>Bagaimana kondisi pekerjaan anda ?</p>'),
(5, 5, '<p><span style=\"color: rgb(33, 37, 41);\">Berapa lama untuk anda mendapatkan pekerjaan setelah lulus ?</span><br></p>'),
(6, 6, '<p>Bagaimana cara anda mendapatkan pekerjaan ?</p>'),
(7, 7, '<p>Seberapa erat hubungan antara bidang studi dengan pekerjaan anda?</p>'),
(8, 10, 'Tingkat pendidikan apa yang paling tepat/sesuai untuk pekerjaan anda saat ini?'),
(9, 9, '<p>Apa jenis perusahaan/instansi/institusi tempat anda bekerja sekarang?<br></p>'),
(10, 8, 'Kira-kira berapa pendapatan anda setiap bulannya?'),
(11, 2, 'Apakah anda bekerja saat ini (termasuk kerja sambilan dan wirausaha)?'),
(12, 90, '<p>Ini Tes Kuesioner</p>');

-- --------------------------------------------------------

--
-- Table structure for table `m_saran`
--

DROP TABLE IF EXISTS `m_saran`;
CREATE TABLE IF NOT EXISTS `m_saran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alumni` int(11) NOT NULL,
  `saran` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_saran`
--

INSERT INTO `m_saran` (`id`, `alumni`, `saran`) VALUES
(2, 18, 'Bang Jamal Ganteng Buanget'),
(3, 25, '<p>Ini Saran Untuk Lembaga</p>');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

DROP TABLE IF EXISTS `m_user`;
CREATE TABLE IF NOT EXISTS `m_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL DEFAULT '0',
  `username` varchar(100) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `dibuat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`id_user`, `nama`, `username`, `password`, `status`, `dibuat`) VALUES
(1, 'Administrator ', 'admin', '$2y$10$AJzLlQTgTNL9Qxdf9MWSG.e6/ixLpaD8UMe2ALptmDniBGXXNE3Rq', 1, '2021-03-06 08:00:09');

-- --------------------------------------------------------

--
-- Table structure for table `riwayat_kerja`
--

DROP TABLE IF EXISTS `riwayat_kerja`;
CREATE TABLE IF NOT EXISTS `riwayat_kerja` (
  `id_rk` int(11) NOT NULL AUTO_INCREMENT,
  `alumni` int(11) DEFAULT NULL,
  `nama_perusahaan` varchar(75) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `t_masuk` varchar(50) DEFAULT NULL,
  `t_keluar` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_rk`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `riwayat_kerja`
--

INSERT INTO `riwayat_kerja` (`id_rk`, `alumni`, `nama_perusahaan`, `jabatan`, `t_masuk`, `t_keluar`) VALUES
(1, 18, 'PT Multifiling Mitra Indonesia', 'Staff Scanner', 'Juli 2019', 'Desember 2019');

-- --------------------------------------------------------

--
-- Table structure for table `riwayat_pendidikan`
--

DROP TABLE IF EXISTS `riwayat_pendidikan`;
CREATE TABLE IF NOT EXISTS `riwayat_pendidikan` (
  `id_rpen` int(11) NOT NULL AUTO_INCREMENT,
  `alumni` int(11) DEFAULT NULL,
  `nama_institusi` varchar(100) NOT NULL DEFAULT '0',
  `tingkat` varchar(50) NOT NULL DEFAULT '0',
  `thn_masuk` varchar(50) NOT NULL DEFAULT '0',
  `thn_lulus` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_rpen`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `riwayat_pendidikan`
--

INSERT INTO `riwayat_pendidikan` (`id_rpen`, `alumni`, `nama_institusi`, `tingkat`, `thn_masuk`, `thn_lulus`) VALUES
(1, 18, 'SDN Harja Mekar 02', 'SD/Sederajat', '2005', '2011'),
(2, 18, 'MTS AS-SALAM', 'SMP/Sederajat', '2011', '2014'),
(3, 18, 'SMK Teknologi As-salam', 'SMA/SMK/Sederajat', '2014', '2017');

-- --------------------------------------------------------

--
-- Table structure for table `t_kuesioner`
--

DROP TABLE IF EXISTS `t_kuesioner`;
CREATE TABLE IF NOT EXISTS `t_kuesioner` (
  `id_tks` int(11) NOT NULL AUTO_INCREMENT,
  `alumni` int(11) NOT NULL,
  `validasi` int(11) NOT NULL DEFAULT '0',
  `diisi` date NOT NULL,
  PRIMARY KEY (`id_tks`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kuesioner`
--

INSERT INTO `t_kuesioner` (`id_tks`, `alumni`, `validasi`, `diisi`) VALUES
(1, 18, 1, '2021-04-08'),
(2, 25, 1, '2021-04-10');

-- --------------------------------------------------------

--
-- Table structure for table `t_kuesioner_detail`
--

DROP TABLE IF EXISTS `t_kuesioner_detail`;
CREATE TABLE IF NOT EXISTS `t_kuesioner_detail` (
  `id_tksd` int(11) NOT NULL AUTO_INCREMENT,
  `tks` int(11) NOT NULL,
  `pertanyaan` int(11) NOT NULL,
  `jawaban` int(11) NOT NULL,
  PRIMARY KEY (`id_tksd`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kuesioner_detail`
--

INSERT INTO `t_kuesioner_detail` (`id_tksd`, `tks`, `pertanyaan`, `jawaban`) VALUES
(1, 1, 1, 1),
(2, 1, 11, 6),
(3, 1, 3, 9),
(4, 1, 2, 11),
(5, 1, 5, 54),
(6, 1, 6, 17),
(7, 1, 7, 27),
(8, 1, 10, 45),
(9, 1, 9, 33),
(10, 1, 8, 37),
(11, 2, 1, 2),
(12, 2, 11, 7),
(13, 2, 3, 8),
(14, 2, 2, 11),
(15, 2, 5, 52),
(16, 2, 6, 15),
(17, 2, 7, 27),
(18, 2, 10, 45),
(19, 2, 9, 35),
(20, 2, 8, 37),
(21, 2, 12, 58);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
