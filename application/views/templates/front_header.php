<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  ================================================== -->
  <meta charset="utf-8">
  <title><?= $title; ?></title>

  <!-- Mobile Specific Metas
  ================================================== -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Bootstrap App Landing Template">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
  <meta name="author" content="Themefisher">
  <meta name="generator" content="Themefisher Small Apps Template v1.0">

  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/images/mic.png" />

  <!-- PLUGINS CSS STYLE -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist2/plugins/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist2/plugins/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist2/plugins/slick/slick.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist2/plugins/slick/slick-theme.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist2/plugins/fancybox/jquery.fancybox.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist2/plugins/aos/aos.css">
  <script src="<?= base_url(); ?>assets/dist2/plugins/sweetalert/sweetalert2.all.min.js"></script>

    <!-- Bootstrap core JavaScript-->
    <!--  plugin JavaScript-->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js" type="text/javascript"></script> -->
    <script src="<?= base_url();?>assets/dist2/backend/dist/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url();?>assets/dist2/backend/dist/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- CUSTOM CSS -->
  <link href="<?php echo base_url();?>assets/dist2/css/style.css" rel="stylesheet">
  <!-- SweetAlert2 -->
</head>
<?php $hal= $this->uri->segment(1);
?>
<body class="body-wrapper" data-spy="scroll" data-target=".privacy-nav">


<nav class="navbar main-nav navbar-expand-lg px-2 px-sm-0 py-2 py-lg-0">
  <div class="container">
    <a class="navbar-brand" href=""><img src="<?php echo base_url();?>assets/images/mic.png" alt="logo" width="10%">&nbsp; STMIK MIC CIKARANG</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
      aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="ti-menu"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item <?= ($hal=="beranda") ? 'active' : '' ;?>">
          <a class="nav-link" href="<?= base_url('beranda');?>">BERANDA</a>
        </li>
        <li class="nav-item <?= ($hal=="info_akademik") ? 'active' : '' ;?>">
          <a class="nav-link" href="<?= base_url('info_akademik');?>">Informasi Akademik</a>
        </li>
        <li class="nav-item <?= ($hal=="info_karir") ? 'active' : '' ;?>">
          <a class="nav-link" href="<?= base_url('info_karir');?>">Info Karir</a>
        </li>
        <li class="nav-item <?= ($hal=="login") ? 'active' : '' ;?>">
          <a class="nav-link" href="<?= base_url('login');?>">LOGIN</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
