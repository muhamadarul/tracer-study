<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/summernote/summernote-bs4.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/daterangepicker/daterangepicker.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- jQuery -->
  <script src="<?= base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
  <script src="<?= base_url(); ?>assets/plugins/jquery/jquery.js"></script>
  <!-- SweetAlert2 -->
  <script src="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert2.all.min.js"></script>
  <link href="<?= base_url(); ?>assets/plugins/datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
</head>
<?php $hal= $this->uri->segment(2);
?>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href=""><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="">
          <span>Hello ! <?php echo $user["nama"]; ?></span>&nbsp;
          <img class="img-profile rounded-circle" src="<?= base_url(); ?>assets/images/default.png" width="25px">
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="<?= base_url(); ?>assets/images/mic.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">TRACER STUDY</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?= base_url('admin/beranda'); ?>" class="nav-link <?= ($hal=="beranda") ? 'active' : '' ;?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Kuesioner
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url('admin/kuesioner'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pertanyaan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('admin/jawaban'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jawaban</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-info-circle"></i>
              <p>
                Informasi
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url('admin/info_akademik'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Info Akademik</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('admin/info_karir'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Info Karir</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="<?= base_url('admin/alumni'); ?>" class="nav-link <?= ($hal=="alumni") ? 'active' : '' ;?>">
              <i class="nav-icon fas fa-graduation-cap"></i>
              <p>
                Alumni
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url('admin/email'); ?>" class="nav-link <?= ($hal=="email") ? 'active' : '' ;?>">
              <i class="nav-icon fas fa-bullhorn"></i>
              <p>
                Broadcast Reminder
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url('admin/laporan_tracer_study'); ?>" class="nav-link <?= ($hal=="laporan_tracer_study") ? 'active' : '' ;?>">
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
                Laporan Tracer Study
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url('admin/laporan_alumni'); ?>" class="nav-link <?= ($hal=="laporan_alumni") ? 'active' : '' ;?>">
              <i class="nav-icon fas fa-file-pdf"></i>
              <p>
                Laporan Alumni
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url('admin/saran'); ?>" class="nav-link <?= ($hal=="saran") ? 'active' : '' ;?>">
              <i class="nav-icon fas fa-file-pdf"></i>
              <p>
                Laporan Saran
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url('admin/user'); ?>" class="nav-link <?= ($hal=="user") ? 'active' : '' ;?>">
              <i class="nav-icon fas fa-user-cog"></i>
              <p>
                User
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url('admin/ubah_password'); ?>" class="nav-link <?= ($hal=="ubah_password") ? 'active' : '' ;?>">
              <i class="nav-icon fas fa-fw fa-key"></i>
              <p>
                Ubah Password
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('admin/auth/logout'); ?>" class="nav-link tombol-logout">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Log Out
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
