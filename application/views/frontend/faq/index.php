<!--=================================
=            FAQ Section            =
==================================-->
<section class="faq section pt-0">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 m-auto">
				<div class="block shadow">
					<!-- Getting Started -->
					<div class="faq-item">
						<!-- Title -->
						<div class="faq-item-title">
							<h2>
								FAQ
							</h2>
						</div>
						<!-- Get Started Accordion -->
						<div id="gstAccordion" data-children=".item">
						  <!-- Accordion Item 01 -->
						  <div class="item">
						  	<div class="item-link">
							    <a data-toggle="collapse" data-parent="#gstAccordion" href="#gstAccordion1">
							      Apa itu Tracer Study?
							    </a>
						  	</div>
						    <div id="gstAccordion1" class="collapse show accordion-block">
						      <p>
						        Tracer Study menurut Ahmad Syafiq (2012:2) yang mengutip dari Schomburg adalah penelitian mengenai lulusan dari suatu perguruan tinggi atau menurut BAN-PT yakni penelusuran alumni untuk menggali informasi melalui pengisian kuesioner yang disusun sedemikian rupa untuk tujuan perbaikan kurikulum dan proses pendidikan di suatu sekolah. Selain itu Tracer Study juga dapat menyediakan informasi mengenai dunia kerja profesional, serta kelengkapan persyaratan bagi akreditasi pendidikan tinggi. 
						      </p>
						    </div>
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--====  End of FAQ Section  ====-->
