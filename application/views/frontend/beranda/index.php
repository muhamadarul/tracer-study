<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
<!--====================================
=            Hero Section            =
=====================================-->
<section class="section gradient-banner">
	<div class="shapes-container">
		<div class="shape" data-aos="fade-down-left" data-aos-duration="1500" data-aos-delay="100"></div>
		<div class="shape" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="100"></div>
		<div class="shape" data-aos="fade-up-right" data-aos-duration="1000" data-aos-delay="200"></div>
		<div class="shape" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200"></div>
		<div class="shape" data-aos="fade-down-left" data-aos-duration="1000" data-aos-delay="100"></div>
		<div class="shape" data-aos="fade-down-left" data-aos-duration="1000" data-aos-delay="100"></div>
		<div class="shape" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="300"></div>
		<div class="shape" data-aos="fade-down-right" data-aos-duration="500" data-aos-delay="200"></div>
		<div class="shape" data-aos="fade-down-right" data-aos-duration="500" data-aos-delay="100"></div>
		<div class="shape" data-aos="zoom-out" data-aos-duration="2000" data-aos-delay="500"></div>
		<div class="shape" data-aos="fade-up-right" data-aos-duration="500" data-aos-delay="200"></div>
		<div class="shape" data-aos="fade-down-left" data-aos-duration="500" data-aos-delay="100"></div>
		<div class="shape" data-aos="fade-up" data-aos-duration="500" data-aos-delay="0"></div>
		<div class="shape" data-aos="fade-down" data-aos-duration="500" data-aos-delay="0"></div>
		<div class="shape" data-aos="fade-up-right" data-aos-duration="500" data-aos-delay="100"></div>
		<div class="shape" data-aos="fade-down-left" data-aos-duration="500" data-aos-delay="0"></div>
	</div>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-6 order-2 order-md-1 text-center text-md-left">
				<h1 class="text-white font-weight-bold mb-4">Tracer Study</h1>
        <h1 class="text-white font-weight-bold mb-4">STMIK MIC CIKARANG</h1>
				<p class="text-white mb-5">
          Selamat datang di Laman Tracer Study STMIK MIC CIKARANG.
        </p>
        <!-- <p class="text-white mb-5">
          Laman ini diperuntukkan untuk mengelola data hasil tracer study yang dilaksanakan oleh STMIK MIC CIKARANG.
        </p> -->
			</div>
			<div class="col-md-6 text-center order-1 order-md-2">
				<img class="img-fluid" src="<?php echo base_url();?>assets/images/mic.png" alt="logo_mic">
			</div>
		</div>
	</div>
</section>
<!--====  End of Hero Section  ====-->

<section class="section pt-0 position-relative pull-top">
	<div class="container">
		<div class="rounded shadow p-5 bg-white">
			<div class="row">
				<div class="col-lg-4 col-md-6 mt-5 mt-md-0 text-center">
					<i class="ti-pin-alt text-primary h1"></i>
					<h3 class="mt-4 text-capitalize h5 ">Visi</h3>
					<p class="regular text-muted">
						Unggul Di Bidang Komputer Tingkat Nasional Tahun 2022
					</p>
				</div>
				<div class="col-lg-4 col-md-6 mt-5 mt-md-0 text-center">
					<i class="ti-bookmark-alt text-primary h1"></i>
					<h3 class="mt-4 text-capitalize h5 ">Misi</h3>
					<p class="regular text-muted">Menyelenggarakan Pendidikan Yang Berkualitas Bidang Komputer, Melaksanakan Penelitian Di Bidang Komputer Yang Inovatif Dan Berorientasi Pada Teknologi Masa Depan, Menyelenggarakan Pengabdian Kepada Masyarakat Dengan Teknologi Tepat Guna Di Bidang Komputer, Mengembangkan Tata Kelola Manajemen Institusi Yang Baik Dan Membangun Lingkungan Akademik Yang Kondusif</p>
				</div>
				<div class="col-lg-4 col-md-12 mt-5 mt-lg-0 text-center">
					<i class="ti-target text-primary h1"></i>
					<h3 class="mt-4 text-capitalize h5 ">Tujuan</h3>
					<p class="regular text-muted">
						Menghasilkan lulusan yang Berkompeten Dan Profesional Yang Mampu Berkompetisi Di Dunia Kerja,
						Menghasilkan Karya Penelitian Dalam Bidang Komputer Yang Bermanfaat Bagi Masyarakat Yang Bertaraf Nasional,
						Melaksanakan Pengabdian Kepada Masyarakat Dalam Bentuk Pembinaan, Bimbingan Dan Konsultasi Bidang Komputer Melibatkan Civitas Akademik,
						Menciptakan Kultur Akademik Yang Inovatif Serta Menjadi Institusi Yang Mandiri Dan Bertata Kelola Manajemen yang Baik
					</p>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<!--==================================
=            Feature Grid            =
===================================-->
<?php foreach ($info_akademik as $row): ?>
	<section class="feature section pt-0">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 ml-auto justify-content-center">
					<!-- Feature Mockup -->
					<div class="image-content" data-aos="fade-right">
						<img class="img-fluid" src="<?php echo base_url();?>assets/images/info/<?= $row['foto'];?>" alt="">
					</div>
				</div>
				<div class="col-lg-6 mr-auto align-self-center">
					<div class="feature-content">
						<!-- Feature Title -->
						<h2>Informasi Akademik</h2>
						<h4><?= $row['judul'];?></h4>
						<!-- Feature Description -->
						<p class="desc">
							<?= substr($row['deskripsi'], 0, 200) . '......'; ?>
						</p>
						</div>
						<!-- Testimonial Quote -->
						<div class="testimonial">
						<a href="<?= base_url('info_akademik/detail/'.$row['id']);?>"><p>Lihat Selengkapnya...</p></a>
							<ul class="list-inline meta">
								<li class="list-inline-item">
									<img src="<?php echo base_url();?>assets/images/default.png" alt="">
								</li>
								<li class="list-inline-item"><?= $row['nama'];?> , <?= date('d/m/Y H:i:s', strtotime($row['diperbarui'])); ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php endforeach; ?>
<?php foreach ($info_karir as $row): ?>
<section class="feature section pt-0">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 ml-auto align-self-center">
				<div class="feature-content">
					<!-- Feature Title -->
					<h2>Informasi Karir</h2>
					<h4><?= $row['judul'];?></h4>
					<!-- Feature Description -->
					<p><?= substr($row['deskripsi'], 0, 200) . '......'; ?></p>
				</div>
				<!-- Testimonial Quote -->
				<div class="testimonial">
				<a href="<?= base_url('info_karir/detail/'.$row['id']);?>"><p>Lihat Selengkapnya...</p></a>

					<ul class="list-inline meta">
						<li class="list-inline-item">
							<img src="<?php echo base_url();?>assets/images/default.png" alt="">
						</li>
						<li class="list-inline-item"><?= $row['nama'];?> , <?= date('d/m/Y H:i:s', strtotime($row['diperbarui'])); ?></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-6 mr-auto justify-content-center">
				<!-- Feature mockup -->
				<div class="image-content" data-aos="fade-left">
					<img class="img-fluid" src="<?php echo base_url();?>assets/images/info_karir/<?= $row['foto'];?>" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
<?php endforeach; ?>
<!--====  End of Feature Grid  ====-->
