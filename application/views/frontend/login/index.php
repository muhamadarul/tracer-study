<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<section class="user-login section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="block">
					<!-- Image -->
					<div class="image align-self-center"><img class="img-fluid" src="<?php echo base_url();?>assets/images/wisuda.jpg"
							alt="desk-image"></div>
					<!-- Content -->
					<div class="content text-center">
						<div class="title-text">
							<h3>Sign in to To Your Account</h3>
						</div>
						<form action="<?= base_url('login'); ?>" method="post">
							<!-- Email -->
							<input class="form-control main" type="email" placeholder="Email" name="email" required>
							<!-- Password -->
							<input class="form-control main" type="password" placeholder="Password" name="password" required>
							<!-- Submit Button -->
							<button class="btn btn-main-sm" type="submit">sign in</button>
						</form>
						<div class="new-acount">
							<!-- <a href="contact.html">Forget your password?</a> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<br>
<br>
