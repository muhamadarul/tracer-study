
<!--================================
=            Page Title            =
=================================-->

<section class="section page-title">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 m-auto">
				<h1>Informasi Akademik</h1>
			</div>
		</div>
	</div>
</section>

<!--====  End of Page Title  ====-->

<!--====  End of Featured Article  ====-->
<section class="post-grid section pt-0">
	<div class="container">
		<div class="row">
      <?php foreach ($info_akademik->result_array() as $row): ?>
        <div class="col-lg-4 col-md-6">
          <!-- Post -->
          <article class="post-sm">
            <!-- Post Image -->
            <div class="post-thumb">
              <a href="<?= base_url('info_akademik/detail/'.$row['id']);?>"><img class="w-100" src="<?php echo base_url();?>assets/images/info/<?= $row['foto'];?>" alt="<?= $row['judul'];?>"></a>
            </div>
            <!-- Post Title -->
            <div class="post-title">
              <h3><a href="<?= base_url('info_akademik/detail/'.$row['id']);?>"><?= $row['judul'];?></a></h3>
            </div>
            <!-- Post Meta -->
            <div class="post-meta">
              <ul class="list-inline post-tag">
                <li class="list-inline-item">
                  <img src="<?php echo base_url();?>assets/images/default.png" alt="author-thumb">
                </li>
                <li class="list-inline-item">
                  <a href="#"><?= $row['nama'];?></a>
                </li>
                <li class="list-inline-item">
                   <?= date('d/m/Y H:i:s', strtotime($row['diperbarui'])); ?>
                </li>
              </ul>
            </div>
            <!-- Post Details -->
            <div class="post-details">
              <p>
                <?= substr($row['deskripsi'], 0, 200) . '......'; ?>
              </p>
            </div>
          </article>
        </div>
      <?php endforeach; ?>
			<div class="col-12">
				<!-- Pagination -->
				  <?php echo $pagination; ?>
			</div>
		</div>
	</div>
</section>
