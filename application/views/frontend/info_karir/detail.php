<?php foreach ($view as $row): ?>
  <section class="section blog-single">
    <div class="container">
      <div class="row">
        <div class="col-md-10 m-auto">
          <!-- Single Post -->
          <article class="single-post">
            <!-- Post title -->
            <div class="post-title text-center">
              <h1><?= $row['judul'];?></h1>
              <!-- Tags -->
              <ul class="list-inline post-tag">
                <li class="list-inline-item">
                  <img src="<?php echo base_url();?>assets/images/default.png" alt="author-thumb">
                </li>
                <li class="list-inline-item">
                  <a href="#"><?= $row['nama'];?></a>
                </li>
                <li class="list-inline-item">
                  <a href="#"><?= date('d/m/Y H:i:s', strtotime($row['diperbarui'])); ?></a>
                </li>
              </ul>
            </div>
            <!-- Post body -->
            <div class="post-body">
              <!-- Feature Image -->
              <div class="feature-image">
                <img class="img-fluid" src="<?php echo base_url();?>assets/images/info_karir/<?= $row['foto'];?>" alt="<?= $row['judul'];?>">
              </div>
              <!-- Paragrapgh -->
              <p>
                <?= $row['deskripsi'];?>
              </p>
            </div>
          </article>
          <!-- About Author Widget -->
        </div>
      </div>
    </div>
  </section>
<?php endforeach; ?>
