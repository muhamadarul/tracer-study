<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Riwayat Pendidikan</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card shadow mb-4">
        <div class="card-header">
          Daftar Pendidikan
          <button data-toggle="modal" data-target="#modalTambah"  class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i>&nbsp; Tambah </button>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Institusi</th>
                  <th>Tingkat</th>
                  <th>Tahun Masuk</th>
                  <th>Tahun Lulus</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody id="tampildata">

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

<!-- Modal Tambah-->
<form id="tambah" method="post">
  <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Tambah Data Pendidikan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center><font color="red"><p id="pesan"></p></font></center>
            <div class="form-group">
              <label class="col-form-label">Nama Institusi:</label>
              <input type="text" class="form-control" id="nama_institusi" name="nama_institusi">
            </div>
            <div class="form-group">
              <label class="col-form-label">Tingkat :</label>
              <select class="form-control" name="tingkat" placeholder="Pilih Tingkat Pendidikan">
                <option value="SD/Sederajat">SD/Sederajat</option>
                <option value="SMP/Sederajat">SMP/Sederajat</option>
                <option value="SMA/SMK/Sederajat">SMA/SMK/Sederajat</option>
                <option value="Diploma">Diploma</option>
                <option value="Sarjana">Sarjana</option>
                <option value="Magister">Magister</option>
                <option value="Doktor">Doktor</option>
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label">Tahun Masuk :</label>
              <input type="text" class="form-control" id="thn_masuk" name="thn_masuk">
            </div>
            <div class="form-group">
              <label class="col-form-label">Tahun Lulus:</label>
              <input type="text" class="form-control" id="thn_lulus" name="thn_lulus">
            </div>
            <div class="form-group" hidden>
              <label class="col-form-label">alumni :</label>
              <input type="text" class="form-control" id="alumni" name="alumni" value="<?= $alumni['id_alumni'];?>">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Modal Ubah-->
<form id="ubah" method="post">
  <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Ubah Pendidikan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center><font color="red"><p id="e_pesan"></p></font></center>
            <div class="form-group" hidden>
              <label class="col-form-label">Id :</label>
              <input type="text" class="form-control" id="id_rpen" name="id_rpen" value="<?= $alumni['id_alumni'];?>">
            </div>
            <div class="form-group">
              <label class="col-form-label">Nama Institusi:</label>
              <input type="text" class="form-control" id="e_nama_institusi" name="e_nama_institusi">
            </div>
            <div class="form-group">
              <label class="col-form-label">Tingkat :</label>
              <select class="form-control" name="e_tingkat" placeholder="Pilih Tingkat Pendidikan">
                <option value="SD/Sederajat">SD/Sederajat</option>
                <option value="SMP/Sederajat">SMP/Sederajat</option>
                <option value="SMA/SMK/Sederajat">SMA/SMK/Sederajat</option>
                <option value="Diploma">Diploma</option>
                <option value="Sarjana">Sarjana</option>
                <option value="Magister">Magister</option>
                <option value="Doktor">Doktor</option>
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label">Tahun Masuk :</label>
              <input type="text" class="form-control" id="e_thn_masuk" name="e_thn_masuk">
            </div>
            <div class="form-group">
              <label class="col-form-label">Tahun Lulus:</label>
              <input type="text" class="form-control" id="e_thn_lulus" name="e_thn_lulus">
            </div>
            <div class="form-group" hidden>
              <label class="col-form-label">alumni :</label>
              <input type="text" class="form-control" id="e_alumni" name="e_alumni" value="<?= $alumni['id_alumni'];?>">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp; Ubah</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
   view();
});
$(function () {
  $("#example1").DataTable();
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "responsive": true,
  });
});
function erase() {
  document.getElementById('nama_institusi').value="";
  document.getElementById('thn_masuk').value="";
  document.getElementById('thn_lulus').value="";
}
$('#tambah').on('submit', function(event){
  event.preventDefault();
      $.ajax({
          type:'POST',
          url:"<?= base_url().'alumni/riwayat_pendidikan/tambah'?>",
          data:$(this).serialize(),
          dataType:'json',
          success:function(data){
            $('#pesan').html(data.pesan);
            if (data.pesan=="") {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil ditambahkan!',
                  type: 'success'
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
              view();
              erase();
            }
          }
    })
});
function edit(id) {
  $.ajax({
    type:"POST",
    data:'id='+id,
    url:'<?= base_url().'alumni/riwayat_pendidikan/getById'?>',
    dataType:'json',
    success:function(data) {
      document.getElementById('id_rpen').value=data.id_rpen;
      document.getElementById('e_nama_institusi').value=data.nama_institusi;
      document.getElementById('e_thn_masuk').value=data.thn_masuk;
      document.getElementById('e_thn_lulus').value=data.thn_lulus;
      $('[name="e_tingkat"]').val(data.tingkat).trigger('change');
    }
  });
}
$('#ubah').on('submit', function(event){
  event.preventDefault();
      $.ajax({
          url:"<?= base_url().'alumni/riwayat_pendidikan/ubah'?>",
          type:"POST",
          data:$(this).serialize(),
          dataType:'json',
          success:function(data){
            $('#e_pesan').html(data.pesan);
            if (data.pesan=='') {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil diubah!',
                  type: 'success'
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
          }
          view();
        }
      })
});
function view() {
  $.ajax({
    type:'POST',
    url:'<?= base_url().'alumni/riwayat_pendidikan/view'?>',
    dataType:'json',
    async:false,
    success:function(data){
      var baris ='';
      var n='';
      var s=[];
      for(var i=0;i<data.length;i++){
        n=i+1;
        if(data[i].tingkat == 'SD/Sederajat') {
           s[i] = '<td>SD/Sederajat</td>';
        }else if (data[i].tingkat == 'SMP/Sederajat'){
           s[i] = '<td>SMP/Sederajat</td>';
        }else if (data[i].tingkat == 'SMA/SMK/Sederajat'){
          s[i] = '<td>SMA/SMK/Sederajat</td>';
        }else if (data[i].tingkat == 'Diploma'){
           s[i] = '<td>Diploma</td>';
        }else if (data[i].tingkat == 'Sarjana'){
            s[i] = '<td>Sarjana</td>';
        }else if (data[i].tingkat == 'Magister'){
             s[i] = '<td>Magister</td>';
        }else if (data[i].tingkat == 'Doktor'){
            s[i] = '<td>Doktor</td>';
        }else {
            s[i] = '<td>Unknown</td>';
        }
        baris += '<tr>'+
                      '<td>'+  n +'</td>'+
                      '<td>'+ data[i].nama_institusi+'</td>'+
                      s[i] +
                      '<td>'+ data[i].thn_masuk+'</td>'+
                      '<td>'+ data[i].thn_lulus+'</td>'+
                      '<td><a onclick="edit('+ data[i].id_rpen+')" data-toggle="modal" data-target="#modalEdit" class="btn btn-default btn-outline-success btn-sm"><i class="fa fa-edit"></i></a><a onclick="hapus('+ data[i].id_rpen+')" class="btn btn-default btn-outline-danger btn-sm"><i class="fa fa-trash"></i></a></td>'+
                 '</tr>';
      }
      $('#tampildata').html(baris);
    }
  });
}
function hapus(id) {
  Swal.fire({
      title: "Apakah anda yakin?",
      text: "data akan dihapus!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          data:'id='+id,
          url:'<?= base_url().'alumni/riwayat_pendidikan/hapus'?>',
          success : function() {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil dihapus!',
                type: 'success'
            });
              view();
          }
        });
      }
  });
}
</script>
