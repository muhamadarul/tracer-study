<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Saran</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card shadow mb-4">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12" id="isi">
              <?php if ($cek == 0): ?>
                <form id="tambah" method="post">
                    <center><font color="red"><p id="pesan"></p></font></center>
                    <div class="form-group">
                      <label class="col-form-label">Berikan saran untuk akademik :</label>
                      <textarea name="saran" class="textarea form-control" id="saran"></textarea>
                    </div>
                    <div class="form-group" hidden>
                      <label class="col-form-label">alumni :</label>
                      <input type="text" class="form-control" id="alumni" name="alumni" value="<?= $alumni['id_alumni'];?>">
                    </div>
                  <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i>&nbsp; Kirim Saran</button>
                </form>
              <?php else: ?>
                  <p>TERIMA KASIH, SARAN ANDA TELAH DIKIRIMKAN</p>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
<script type="text/javascript">
$(document).ready(function() {
   $('.textarea').summernote();
});
$(function () {
  $("#example1").DataTable();
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "responsive": true,
    "processing": true,

  });
});
$('#tambah').on('submit', function(event){
  event.preventDefault();
      $.ajax({
          type:'POST',
          url:"<?= base_url().'alumni/saran/tambah'?>",
          data:$(this).serialize(),
          dataType:'json',
          success:function(data){
            $('#pesan').html(data.pesan);
            if (data.pesan=="") {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil ditambahkan!',
                  type: 'success'
              });
              $("#tambah").hide();
              var p = '<p>TERIMA KASIH, SARAN ANDA TELAH DIKIRIMKAN</p>';
                $('#isi').html(p);
            }
          }
    })
});


</script>
