
<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Kuesioner</h1>
  </div>
  <!-- Content Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card shadow mb-4">
        <div class="card-header">
          <?php if ($cek == 0): ?>
            Kuesioner anda
            <a href="#" onclick="tambahkuesioner()"id="btnTambah" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i>&nbsp; Tambah</a>
           <?php else: ?>
            kuesioner anda
          <?php endif; ?>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12" id="isi2">
              <?php if ($validasi['validasi'] == 1): ?>
                <p>TERIMA KASIH TELAH MENGISI KUESIONER</p>
              <?php endif; ?>
              <?php if ($cek != 0 && $validasi['validasi'] != 1): ?>
              <a href="" id="isi" data-toggle="modal" data-target="#modal" data-whatever="@getbootstrap" class="btn btn-success"><i class="fa fa-edit">&nbsp; Isi Kuesioner</i></a>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
<!-- Large modal -->
<form class="" method="post" id="kuesioner">
  <div class="modal fade" id="modal"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">KUESIONER</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
              <?php
              $result = array();
              foreach ($jawaban as $element) {
                $result[$element['pertanyaan']][] = $element;
              }
              ?>
              <div class="col-md-12">
              <?php foreach ($result as $key => $value): ?>
              <table border="0">
                <tr>
                  <td>
                    <strong><p><?= $key;?></p></strong>
                  </td>
                </tr>
                <?php foreach ($value as $k => $v): ?>
                <tr>
                  <td>
                    <input type="radio" name="<?= $v['kuesioner'];?>" value="<?= $v['id_jawaban'];?>" required>&nbsp;<?= $v['jawaban'];?>
                  </td>
                </tr>
                <?php endforeach; ?>
                <?php endforeach; ?>
              </table>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">

$('#kuesioner').on('submit', function(event){
  event.preventDefault();
  Swal.fire({
      title: "Perhatian",
      text: "Anda tidak dapat mengubah jawaban kuesioner, simpan sekarang?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Simpan !'
  }).then((result) => {
      if (result.value) {
        $.ajax({
            url:"<?= base_url().'alumni/kuesioner/save'?>",
            type:"POST",
            data:$(this).serialize(),
            dataType:'json',
            success:function(data){
                Swal.fire({
                    title: 'Berhasil',
                    text: 'Kuesioner Telah Diisi!',
                    type: 'success'
                });
                setTimeout(function () {
                  $("[data-dismiss=modal]").trigger({
                    type: "click"
                  });
                },100)
                 $("#isi").hide();
                 var p = '<p>TERIMA KASIH TELAH MENGISI KUESIONER</p>';
                   $('#isi2').html(p);
            }
        })
      }
  });
});

function tambahkuesioner() {
  Swal.fire({
      title: "Perhatian",
      text: "Anda harus mengisi kuesioner ini dengan sebenernya-benarnya!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Isi !'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          url:'<?= base_url().'alumni/kuesioner/tambah'?>',
          success : function() {
            Swal.fire({
                title: 'Berhasil',
                text: 'Kuesioner Ditambahkan!',
                type: 'success'
            });
             $("#btnTambah").hide();
             var isi = '<a href="" id="isi" data-toggle="modal" data-target="#modal" data-whatever="@getbootstrap" class="btn btn-success"><i class="fa fa-edit">&nbsp; Isi Kuesioner</i></a>';
               $('#isi2').html(isi);

          }
        });
      }
  });
}
</script>
