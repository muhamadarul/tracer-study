<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Data Diri Alumni</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card shadow mb-4">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="text-left">
                <table class="table  table-striped">
                  <thead>
                    <tr>
                      <td>Nama</td>
                      <td>:</td>
                      <td><?= $alumni['nama']; ?></td>
                    </tr>
                    <tr>
                      <td>Tempat, Tgl Lahir</td>
                      <td>:</td>
                      <td><?= $alumni['tmp_lahir']; ?>, <?= date('d F Y', strtotime($alumni['tgl_lahir'])) ?></td>
                    </tr>
                    <tr>
                      <td>Jenis Kelamin</td>
                      <td>:</td>
                      <td><?= $alumni['jk']; ?></td>
                    </tr>
                    <tr>
                      <td>Agama</td>
                      <td>:</td>
                      <td><?= $alumni['agama']; ?></td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>:</td>
                      <td><?= $alumni['email']; ?></td>
                    </tr>
                    <tr>
                      <td>No Telepon</td>
                      <td>:</td>
                      <td><?= $alumni['tlp']; ?></td>
                    </tr>
                    <tr>
                      <td>Tahun Lulus</td>
                      <td>:</td>
                      <td><?= $alumni['thn_lulus']; ?></td>
                    </tr>
                    <tr>
                      <td>Jurusan</td>
                      <td>:</td>
                      <?php if ($alumni['jurusan']==2): ?>
                      <td>Teknik Informatika</td>
                      <?php else: ?>
                      <td>Sistem Informasi</td>
                      <?php endif; ?>
                    </tr>
                    <tr>
                      <td>Alamat</td>
                      <td>:</td>
                      <td><?= $alumni['alamat']; ?></td>
                    </tr>
                    <tr>
                      <td>Foto</td>
                      <td>:</td>
                      <td>
                        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width:15rem;" src="<?= base_url(); ?>assets/images/alumni/<?= $alumni['foto']; ?>" alt="">
                      </td>
                    </tr>
                  </thead>
                </table>
                <a href="<?= base_url('alumni/profile/ubah');?>" class="btn btn-success"><i class="fa fa-pen"></i> UBAH DATA</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
