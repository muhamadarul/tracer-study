<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Ubah Data Alumni</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card shadow mb-4">
        <div class="card-body">
          <?php if (validation_errors()) : ?>
              <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                  <?= validation_errors(); ?>
              </div>
          <?php endif; ?>
          <div class="row">
            <div class="col-md-6">
              <div class="text-left">
                <?php echo form_open_multipart('alumni/profile/ubah'); ?>
                <div class="form-group">
                  <label>Nama :</label>
                  <input type="text" class="form-control" name="nama" value="<?=$view['nama'];?>" readonly>
                </div>
                <div class="form-group">
                  <label>Tempat Lahir :</label>
                  <input type="text" class="form-control" name="tmp_lahir" value="<?=$view['tmp_lahir'];?>" readonly>
                </div>
                <div class="form-group">
                  <label>Tgl Lahir :</label>
                  <input type="text" class="form-control" name="tgl_lahir" value="<?= date('d F Y', strtotime($view['tgl_lahir'])) ?>" readonly>
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin :</label>
                  <input type="text" class="form-control" name="jk" value="<?=$view['jk'];?>" readonly>
                </div>
                <div class="form-group">
                  <label>Agama :</label>
                  <input type="text" class="form-control" name="agama" value="<?=$view['agama'];?>" readonly>
                </div>
                <div class="form-group">
                  <label>Email :</label>
                  <input type="text" class="form-control" name="email" value="<?=$view['email'];?>" readonly>
                </div>
                <div class="form-group">
                  <label>Tahun Lulus :</label>
                  <input type="text" class="form-control" name="thn_lulus" value="<?=$view['thn_lulus'];?>" readonly>
                </div>
                <div class="form-group">
                  <label>Jurusan :</label>
                  <input type="text" class="form-control" name="jurusan" value="<?=$view['nama_jurusan'];?>" readonly>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="text-left">
                <div class="form-group">
                  <label>Telepon :</label>
                  <input type="text" class="form-control" name="tlp" value="<?=$view['tlp'];?>" required>
                </div>
                <div class="form-group">
                  <label>Alamat :</label>
                  <textarea name="alamat" class="form-control" rows="8" cols="80" required><?=$view['alamat'];?></textarea>
                </div>
                <div class="form-group">
                  <label>Foto : </label>
                  <input type="file" class="form-control" name="foto" value="">
                </div>
                <input type="text" class="form-control" name="foto_lama" value="<?=$view['foto'];?>" hidden>
                <input type="text" class="form-control" name="id" value="<?=$view['id_alumni'];?>" hidden>
                <button type="submit" class="btn btn-primary" name="simpan"><i class="fa fa-save"></i> SIMPAN</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
