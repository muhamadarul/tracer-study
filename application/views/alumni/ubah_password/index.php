<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Ubah Data Alumni</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card shadow mb-4">
        <div class="card-body">
          <?php if (validation_errors()) : ?>
              <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                  <?= validation_errors(); ?>
              </div>
          <?php endif; ?>
          <div class="row">
            <div class="col-md-6">
              <div class="text-left">
                <?= $this->session->flashdata('message');?>
                <?php echo form_open_multipart('alumni/ubah_password'); ?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Password Lama</label>
                        <input name="password_lama" type="password" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Password">
                    </div>
                    <?= form_error('password_lama');?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Password Baru</label>
                        <input name="password_baru1" type="password" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Password">
                    </div>
                    <?= form_error('password_baru1');?>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Ulang Password</label>
                        <input name="password_baru2" type="password" class="form-control" id="exampleInputEmail1" placeholder="Ulangi Password">
                    </div>
                    <?= form_error('password_baru2');?>
                    <input name="id" type="hidden" value="<?= $alumni['id_alumni']; ?>">
                <button type="submit" class="btn btn-primary" name="simpan"><i class="fa fa-save"></i> SIMPAN</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
