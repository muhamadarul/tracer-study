<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Hasil Tracer Study</h1><br>
          <button  onclick="window.print();" class="btn btn-danger"><i class="fa fa-download"></i>&nbsp; Cetak</button>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <!-- <li class="breadcrumb-item active">Dashboard</li> -->
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-4">
          <!-- PIE CHART -->
          <div class="card card-danger">
            <div class="card-header">
              <h3 class="card-title">Jumlah yang Mengisi Kuesioner</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <canvas id="pieMengisi" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-lg-4">
          <!-- PIE CHART -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Aktivitas Alumni Setelah Lulus</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <canvas id="aktivitas" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- ./col -->
        <div class="col-lg-4">
          <!-- PIE CHART -->
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Persentase Lulusan yang Bekerja/Berwirausaha</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <canvas id="cKerjaUsaha" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-4">
          <!-- PIE CHART -->
          <div class="card card-warning">
            <div class="card-header">
              <h3 class="card-title">Persentase Kondisi Pekerjaan Alumni</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <canvas id="kondisipekerjaan" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- ./col -->
        <div class="col-lg-4">
          <div class="card card-success">
            <div class="card-header">
              <h3 class="card-title"> Korelasi Bidang Ilmu Dengan Pekerjaan</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="chart">
                <canvas id="korelasi" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-lg-4">
          <div class="card card-danger">
            <div class="card-header">
              <h3 class="card-title"> Waktu Tunggu Lulusan Mendapatkan Pekerjaan</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="chart">
                <canvas id="waktutunggu" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-4">
          <div class="card card-danger">
            <div class="card-header">
              <h3 class="card-title"> Kesesuaian Tingkat Pendidikan dengan Pekerjaan</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="chart">
                <canvas id="tingkatpendidikan" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-lg-8">
          <!-- BAR CHART -->
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Pendapatan Alumni Perbulan</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
      </div>
      <!-- /.row -->
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Cara Lulusan Mendapatkan Pekerjaan</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="chart">
                <canvas id="dapatkerja" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- ChartJS -->
<script src="<?= base_url(); ?>assets/plugins/chart.js/Chart.min.js"></script>
<script type="text/javascript">
// Grafik Pendapatan Alumni
var barPendapatan = $('#barChart').get(0).getContext('2d')
   var barPendapatanOptions = {
     responsive              : true,
     maintainAspectRatio     : false,
     datasetFill             : false
   }

   var pendapatan = new Chart(barPendapatan, {
     type: 'bar',
     options: barPendapatanOptions,
     data: {
         labels: [
           <?php
             if (count($jawaban)>0) {
               foreach ($jawaban as $data) {
                 echo "'" .$data['jawaban'] ."',";
               }
             }
           ?>
         ],
         datasets: [{
             label: 'Jumlah Alumni',
             backgroundColor: '#ADD8E6',
             borderColor: '##93C3D2',
             data: [<?php foreach ($gaji as $row) {echo "'" .$row."',";} ?>
             ]
         }]
     },
 });
 // Grafik Isi Kuesioner
 var pieMengisi = $('#pieMengisi').get(0).getContext('2d')
    var pieMengisiOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    var dataMengisi = {
      labels: ['Mengisi %', 'Tidak Mengisi %'],
      datasets:[{
        label:"Jumlah Mengisi",
        data:[<?php foreach ($isi as $row) {echo "'" .$row."',";} ?>],
        backgroundColor:[
           '#00a65a','#f56954',
        ]
      }]
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var mengisi = new Chart(pieMengisi, {
      type: 'pie',
      data: dataMengisi,
      options: pieMengisiOptions
    })
//Grafik Aktivitas Alumni
var donutAktivitas = $('#aktivitas').get(0).getContext('2d')
   var dataAktivitas = {
     labels: [
         'Bekerja %',
         'Tidak Bekerja %',
         'Belum Bekerja %',
         'Berwirausaha %',
         'Melanjutkan Study %',
     ],
     datasets: [
       {
         data: [<?php foreach ($aktivitas as $row) {echo "'" .$row."',";} ?>],
         backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc'],
       }
     ]
   }
   var aktivitasOptions     = {
     maintainAspectRatio : false,
     responsive : true,
   }
   var aktivitas = new Chart(donutAktivitas, {
     type: 'doughnut',
     data: dataAktivitas,
     options: aktivitasOptions
   })

// Grafik Lulusan yang Bekerja/Berwirausaha
var cKerjaUsaha = $('#cKerjaUsaha').get(0).getContext('2d')
   var cKerjaUsahaOptions     = {
     maintainAspectRatio : false,
     responsive : true,
   }
   var datacKerjaUsaha = {
     labels: ['Bekerja Sesuai Bidang Ilmu %', 'Bekerja Tidak Sesuai Bidang Ilmu %', 'Wirausaha %'],
     datasets:[{
       label:"Jumlah",
       data:[<?php foreach ($kerjausaha as $row) {echo "'" .$row."',";} ?>],
       backgroundColor:[
          '#f39c12', '#00c0ef', '#3c8dbc'
       ]
     }]
   };
   //Create pie or douhnut chart
   // You can switch between pie and douhnut using the method below.
   var KerjaUsaha = new Chart(cKerjaUsaha, {
     type: 'pie',
     data: datacKerjaUsaha,
     options: cKerjaUsahaOptions
   })

//Grafik Kondisi Pekerjaan Alumni
var cKondisiKerja = $('#kondisipekerjaan').get(0).getContext('2d')
  var dataKondisiKerja = {
    labels: [
        'Paruh Waktu %',
        'Penuh Waktu %',
    ],
    datasets: [
      {
        data: [<?php foreach ($kondisiKerja as $row) {echo "'" .$row."',";} ?>],
        backgroundColor : ['#00a65a','#00c0ef'],
      }
    ]
  }
  var kondisiKerjaOptions     = {
    maintainAspectRatio : false,
    responsive : true,
  }
  var kondisiKerja = new Chart(cKondisiKerja, {
    type: 'doughnut',
    data: dataKondisiKerja,
    options: kondisiKerjaOptions
  })
  // Grafik Korelasi Bidang Ilmu Dengan Pekerjaan
  var barKorelasi = $('#korelasi').get(0).getContext('2d')
     var barKorelasiOptions = {
       responsive              : true,
       maintainAspectRatio     : false,
       datasetFill             : false
     }

     var korelasi = new Chart(barKorelasi, {
       type: 'bar',
       options: barKorelasiOptions,
       data: {
           labels: ['Sangat Sesuai', 'Sesuai', 'Sedang', 'Tidak Sesuai', 'Sangat Tidak Sesuai'],
           datasets: [{
               label: 'Jumlah Alumni',
               backgroundColor: '#00c0ef',
               borderColor: '##93C3D2',
               data: [<?php foreach ($korelasi as $row) {echo "'" .$row."',";} ?>
               ]
           }]
       },
   });

   // Grafik Waktu Tunggu
   var barWaktuTunggu = $('#waktutunggu').get(0).getContext('2d')
      var barWaktuTunggOptions = {
        responsive              : true,
        maintainAspectRatio     : false,
        datasetFill             : false
      }

      var waktutunggu = new Chart(barWaktuTunggu, {
        type: 'bar',
        options: barWaktuTunggOptions,
        data: {
            labels: ['Kurang dari 1 Bulan', '1-2 Bulan', '3-6 Bulan', '7-12 Bulan', '1-2 Tahun', 'Lebih dari 2 Tahun'],
            datasets: [{
                label: 'Jumlah Alumni',
                backgroundColor: '#3c8dbc',
                borderColor: '##93C3D2',
                data: [<?php foreach ($waktutunggu as $row) {echo "'" .$row."',";} ?>
                ]
            }]
        },
    });

    // Grafik Kesesuaian Tingkat Pendidikan
    var barTingkatPendidikan = $('#tingkatpendidikan').get(0).getContext('2d')
       var barTingkatPendidikanOptions = {
         responsive              : true,
         maintainAspectRatio     : false,
         datasetFill             : false
       }

       var tingkatpendidikan = new Chart(barTingkatPendidikan, {
         type: 'bar',
         options: barTingkatPendidikanOptions,
         data: {
             labels: ['Setingkat Lebih Tinggi', 'Tingkat yang Sama', 'Setingkat Lebih Rendah', '	Tidak Perlu Pendidikan Tinggi'],
             datasets: [{
                 label: 'Jumlah Alumni',
                 backgroundColor: '#00a65a',
                 borderColor: '##93C3D2',
                 data: [<?php foreach ($tingkatpendidikan as $row) {echo "'" .$row."',";} ?>
                 ]
             }]
         },
     });


     // Grafik Cara Mendapat Pekerjaan
     var barDapatKerja = $('#dapatkerja').get(0).getContext('2d')
        var barDapatKerjaOptions = {
          responsive              : true,
          maintainAspectRatio     : false,
          datasetFill             : false
        }

        var dapatkerja = new Chart(barDapatKerja, {
          type: 'bar',
          options: barDapatKerjaOptions,
          data: {
              labels: [
                'Melalui iklan di koran/majalah, brosur',
                'Melamar ke perusahaan tanpa mengetahui lowongan yang ada',
                'Pergi ke bursa/pameran kerja',
                'Mencari lewat internet/iklan online/milis',
                'Dihubungi oleh perusahaan',
                'Menghubungi Kemenakertrans',
                'Menghubungi agen tenaga kerja komersial/swasta',
                'Memeroleh informasi dari pusat/kantor pengembangan karir fakultas/universitas',
                'Menghubungi kantor kemahasiswaan/hubungan alumni',
                'Membangun jejaring (network) sejak masih kuliah',
                'Melalui relasi (misalnya dosen, orang tua, saudara, teman, dll.)',
                'Melalui penempatan kerja atau magang ',
                'Bekerja di tempat yang sama dengan tempat kerja semasa kuliah'
                ],
              datasets: [{
                  label: 'Jumlah Alumni',
                  backgroundColor: '#f56954',
                  borderColor: '##93C3D2',
                  data: [<?php foreach ($dapatkerja as $row) {echo "'" .$row."',";} ?>
                  ]
              }]
          },
      });
</script>
