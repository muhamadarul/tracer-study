<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Ubah Password</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Ubah Password</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
              <div class="card-header">
                  <h3 class="card-title">Ubah Password Anda</h3>
              </div>
                    <div class="card-body table-responsive">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>
                        <?= $this->session->flashdata('message');?>
                        <?php echo form_open_multipart('admin/ubah_password'); ?>
                          <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Password Lama</label>
                                <input name="password_lama" type="password" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Password">
                            </div>
                            <?= form_error('password_lama');?>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Password Baru</label>
                                <input name="password_baru1" type="password" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Password">
                            </div>
                            <?= form_error('password_baru1');?>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Ulang Password</label>
                                <input name="password_baru2" type="password" class="form-control" id="exampleInputEmail1" placeholder="Ulangi Password">
                            </div>
                            <?= form_error('password_baru2');?>
                            <input name="id" type="hidden" value="<?= $user['id_user']; ?>">
                          <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                        </div>
                  </div>
            </div>
        </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
