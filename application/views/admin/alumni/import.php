<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Kelola Alumni  &nbsp; <i class="fas fa-graduation-cap"></i></h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>
            <div class="card-header">
              <h3 class="card-title">Import Data Alumni</h3>
              <a href="<?= base_url('admin/alumni/exportFileContoh'); ?>" class="btn btn-success btn-sm float-right"><i class="fa fa-download"></i>&nbsp; Download Format</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <?= form_open_multipart('admin/alumni/import'); ?>
              <div class="form-group" style="width:30%;">
                <label class="col-form-label">Jurusan :</label>
                <select class="form-control" name="jurusan">
                  <option value="">-- Pilih Jurusan --</option>
                  <?php foreach ($jurusan as $row): ?>
                  <option value="<?= $row['id_jurusan'];?>"><?= $row['nama_jurusan'];?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group" style="width:30%;">
                <input type="file" class="form-control" name="file" />
                <p>* Gunakan file dengan extensi .xlsx</p>
                <button type="submit" class="btn btn-primary" name="submit" value="upload">Upload... </button>
              </div>
              <?= form_close(); ?>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
