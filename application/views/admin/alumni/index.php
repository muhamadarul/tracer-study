<style>
  .ui-datepicker-calendar{
    display: none;
  }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Kelola Alumni  &nbsp; <i class="fas fa-graduation-cap"></i></h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
               <button class="btn btn-primary btn-sm float-right" onclick="add()"><i class="fa fa-plus"></i>&nbsp; Tambah</button>
              <a href="<?= base_url('admin/alumni/import'); ?>" class="btn btn-success btn-sm float-left"><i class="fa fa-upload"></i>&nbsp; Import Data</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="table" class="table table-bordered table-striped dt-responsive nowrap">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Tempat Lahir</th>
                  <th>Tgl Lahir</th>
                  <th>Email</th>
                  <th>Jurusan</th>
                  <th>Tahun Lulus</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- modal Edit -->
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_update" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
              <div class="modal-body form">
                  <p>*) Isian wajib diisi !</p>
                <br>
               <center><font color="red"><p id="e_pesan"></p></font></center>
                  <form action="#" id="form_update" class="form-horizontal">
                    <div class="row">
                      <div class="col-md-6">
                        <input type="hidden" value="" name="id"/>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama *</label>
                                <div class="col-md-12">
                                    <input name="nama2" id="nama2" placeholder="Nama Lengkap" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tempat Lahir *</label>
                                <div class="col-md-12">
                                    <input name="tmp_lahir2" id="tmp_lahir2" placeholder="Tempat Lahir" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tanggal Lahir *</label>
                                <div class="col-md-12">
                                    <input name="tgl_lahir2" id="tgl_lahir2" class="form-control datepicker" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Jenis Kelamin *</label>
                                <div class="col-md-12">
                                    <select name="jk2" id="jk2" class="form-control">
                                        <option value="">--Pilih Jenis Kelamin--</option>
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Agama *</label>
                                <div class="col-md-12">
                                    <select name="agama2" id="agama2" class="form-control">
                                        <option value="">--Pilih Agama--</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Protestan">Protestan</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Buddha">Buddha</option>
                                        <option value="Khonghucu">Khonghucu</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Alamat *</label>
                                <div class="col-md-12">
                                    <textarea name="alamat2" id="alamat2" placeholder="Alamat" class="form-control"></textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Email *</label>
                                <div class="col-md-12">
                                    <input name="email2" id="email2" placeholder="Email" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Telepon *</label>
                                <div class="col-md-12">
                                    <input name="tlp2" id="tlp2" placeholder="No Telepon" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tahun Lulus *</label>
                                <div class="col-md-12">
                                    <input name="thn_lulus2" id="thn_lulus2" class="form-control date-own" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Jurusan *</label>
                                <div class="col-md-12">
                                    <select name="jurusan2" id="jurusan2" class="form-control">

                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Password </label><p>Isi Jika ingin mengubah password</p>
                                <div class="col-md-12">
                                    <input name="password2" id="password2" class="form-control" type="password">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Foto </label><p>Isi Jika ingin mengubah foto</p>
                                <div class="col-md-12">
                                    <input name="foto2" id="foto2" class="form-control" type="file">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group" hidden>
                                <label class="control-label col-md-3">Foto</label>
                                <div class="col-md-12">
                                    <input name="foto_lama" id="foto_lama" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    </div>
                  </form>
              <div class="modal-footer">
                  <button type="button" id="btnUpdate" onclick="update()" class="btn btn-primary">Update</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- modal tambah -->
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
              <div class="modal-body form">
                  <p>*) Isian wajib diisi !</p>
                <br>
               <center><font color="red"><p id="pesan"></p></font></center>
                  <form action="#" id="form" class="form-horizontal">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama *</label>
                                <div class="col-md-12">
                                    <input name="nama" id="nama" placeholder="Nama Lengkap" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tempat Lahir *</label>
                                <div class="col-md-12">
                                    <input name="tmp_lahir" id="tmp_lahir" placeholder="Tempat Lahir" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tanggal Lahir *</label>
                                <div class="col-md-12">
                                    <input name="tgl_lahir" id="tgl_lahir" class="form-control datepicker" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Jenis Kelamin *</label>
                                <div class="col-md-12">
                                    <select name="jk" id="jk" class="form-control">
                                        <option value="">--Pilih Jenis Kelamin--</option>
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Agama *</label>
                                <div class="col-md-12">
                                    <select name="agama" id="agama" class="form-control">
                                        <option value="">--Pilih Agama--</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Protestan">Protestan</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Buddha">Buddha</option>
                                        <option value="Khonghucu">Khonghucu</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Alamat *</label>
                                <div class="col-md-12">
                                    <textarea name="alamat" id="alamat" placeholder="Alamat" class="form-control"></textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Email *</label>
                                <div class="col-md-12">
                                    <input name="email" id="email" placeholder="Email" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Telepon *</label>
                                <div class="col-md-12">
                                    <input name="tlp" id="tlp" placeholder="No Telepon" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Password *</label>
                                <div class="col-md-12">
                                    <input name="password" id="password" class="form-control" type="password">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tahun Lulus *</label>
                                <div class="col-md-12">
                                    <input name="thn_lulus" id="thn_lulus" class="form-control date-own" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Jurusan *</label>
                                <div class="col-md-12">
                                    <select name="jurusan" id="jurusan" class="form-control">

                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Foto</label>
                                <div class="col-md-12">
                                    <input name="foto" id="foto" class="form-control" type="file">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    </div>
                  </form>
              <div class="modal-footer">
                  <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- modal view -->
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_view" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
              <div class="modal-body form">
                <br>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama</label>
                                <div class="col-md-12">
                                    <input name="nama3" id="nama3" placeholder="Nama Lengkap" class="form-control" type="text" readonly>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tempat Lahir *</label>
                                <div class="col-md-12">
                                    <input name="tmp_lahir3" id="tmp_lahir3" placeholder="Tempat Lahir" class="form-control" type="text" readonly>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tanggal Lahir *</label>
                                <div class="col-md-12">
                                    <input name="tgl_lahir3" id="tgl_lahir3" class="form-control datepicker" type="text" readonly>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Jenis Kelamin *</label>
                                <div class="col-md-12">
                                    <select name="jk3" id="jk3" class="form-control" readonly>
                                        <option value="">--Pilih Jenis Kelamin--</option>
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Agama *</label>
                                <div class="col-md-12">
                                    <select name="agama3" id="agama3" class="form-control" readonly>
                                        <option value="">--Pilih Agama--</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Protestan">Protestan</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Buddha">Buddha</option>
                                        <option value="Khonghucu">Khonghucu</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Alamat *</label>
                                <div class="col-md-12">
                                    <textarea name="alamat3" id="alamat3" placeholder="Alamat" class="form-control" readonly></textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Email *</label>
                                <div class="col-md-12">
                                    <input name="email3" id="email3" placeholder="Email" class="form-control" type="text" readonly>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Telepon *</label>
                                <div class="col-md-12">
                                    <input name="tlp3" id="tlp3" placeholder="No Telepon" class="form-control" type="text" readonly>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tahun Lulus *</label>
                                <div class="col-md-12">
                                    <input name="thn_lulus3" id="thn_lulus3" class="form-control date-own" type="text" readonly>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Jurusan *</label>
                                <div class="col-md-12">
                                    <select name="jurusan3" id="jurusan3" class="form-control" readonly>

                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Foto</label>
                                <div class="col-md-12" id="viewfoto">

                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    </div>
                  </form>
              <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="<?= base_url(); ?>assets/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
var save_method; //for save method string
var table;
$(document).ready(function() {
  select();
    //datatables
    table = $('#table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "responsive": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('admin/alumni/ajax_list')?>",
            "type": "POST",
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    });
});
$('.datepicker').datepicker({
       autoclose: true,
       format: "yyyy-mm-dd",
       todayHighlight: true,
       orientation: "top auto",
       todayBtn: true,
       todayHighlight: true,
});
$('.date-own').datepicker({
  minViewMode:2,
  format:'yyyy'
});

function add()
{

    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Tambah Alumni'); // Set Title to Bootstrap modal title
}

function eraseText() {
     document.getElementById("nama").value = "";
     document.getElementById("tmp_lahir").value = "";
     document.getElementById("tgl_lahir").value = "";
     document.getElementById("jk").value = "";
     document.getElementById("agama").value = "";
     document.getElementById("alamat").value = "";
     document.getElementById("email").value = "";
     document.getElementById("tlp").value = "";
     document.getElementById("password").value = "";
     document.getElementById("foto").value = "";
     document.getElementById("thn_lulus").value = "";
     document.getElementById("jurusan").value = "";
	}
  function eraseTextEdit() {
       document.getElementById("nama2").value = "";
       document.getElementById("tmp_lahir2").value = "";
       document.getElementById("tgl_lahir2").value = "";
       document.getElementById("jk2").value = "";
       document.getElementById("agama2").value = "";
       document.getElementById("alamat2").value = "";
       document.getElementById("email2").value = "";
       document.getElementById("tlp2").value = "";
       document.getElementById("password2").value = "";
       document.getElementById("foto2").value = "";
       document.getElementById("thn_lulus2").value = "";
       document.getElementById("jurusan2").value = "";
  	}
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable
    var url;
    url = "<?php echo site_url('admin/alumni/tambah')?>";
    // var file_data = $('#foto').prop('files')[0];
    var form_data = new FormData(document.getElementById("form"));
    // form_data.append('file', file_data);

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: form_data,
        dataType: "JSON",
        contentType: false,
        processData:false,
        cache:false,
        success: function(data)
        {
          console.log(data);
            $('#pesan').html(data.pesan);
            if(data.pesan=="") //if success close modal and reload ajax table
            {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil ditambahkan!',
                  type: 'success'
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
                reload_table();
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
        }
    });
}
function edit(id)
{

    $('#form_update')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('admin/alumni/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id_alumni);
            $('[name="nama2"]').val(data.nama);
            $('[name="tmp_lahir2"]').val(data.tmp_lahir);
            $('[name="tgl_lahir2"]').val(data.tgl_lahir);
            $('[name="agama2"]').val(data.agama);
            $('[name="alamat2"]').val(data.alamat);
            $('[name="email2"]').val(data.email);
            $('[name="tlp2"]').val(data.tlp);
            $('[name="thn_lulus2"]').val(data.thn_lulus);
            $('[name="jk2"]').val(data.jk);
            $('[name="jurusan2"]').val(data.jurusan);
            $('[name="foto_lama"]').val(data.foto);
            $('#modal_form_update').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Alumni'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
function detail(id)
{
  url = "<?php echo base_url()?>assets/images/alumni";
    $.ajax({
        url : "<?php echo site_url('admin/alumni/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="nama3"]').val(data.nama);
            $('[name="tmp_lahir3"]').val(data.tmp_lahir);
            $('[name="tgl_lahir3"]').val(data.tgl_lahir);
            $('[name="agama3"]').val(data.agama);
            $('[name="alamat3"]').val(data.alamat);
            $('[name="email3"]').val(data.email);
            $('[name="tlp3"]').val(data.tlp);
            $('[name="thn_lulus3"]').val(data.thn_lulus);
            $('[name="jk3"]').val(data.jk);
            $('[name="jurusan3"]').val(data.jurusan);
             $('#viewfoto').html('<img src="'+url+'/'+data.foto+'" width="125" height="150" />');
            $('[name="foto3"]').val(data.foto);
            $('#modal_view').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Detail Alumni'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
function hapus(id) {
  Swal.fire({
      title: "Apakah anda yakin?",
      text: "data akan dihapus!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          data:'id='+id,
          url:'<?= base_url().'admin/alumni/hapus'?>',
          success : function() {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil dihapus!',
                type: 'success'
            });
              reload_table();
          }
        });
      }
  });
}
function update() {
  $('#btnUpdate').text('updating...'); //change button text
  $('#btnUpdate').attr('disabled',true); //set button disable
  var url;
  url = "<?php echo site_url('admin/alumni/ubah')?>";
  // var file_data = $('#foto').prop('files')[0];
  var form_data2 = new FormData(document.getElementById("form_update"));
  // form_data.append('file', file_data);

  // ajax adding data to database
  $.ajax({
      url : url,
      type: "POST",
      data: form_data2,
      dataType: "JSON",
      contentType: false,
      processData:false,
      cache:false,
      success: function(data)
      {
        console.log(data);
          $('#e_pesan').html(data.pesan);
          if(data.pesan=="") //if success close modal and reload ajax table
          {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil diubah!',
                type: 'success'
            });
            setTimeout(function () {
              $("[data-dismiss=modal]").trigger({
                type: "click"
              });
            },100)
              reload_table();
              $('#btnUpdate').text('save'); //change button text
              $('#btnUpdatex').attr('disabled',false); //set button enable
          }
          $('#btnUpdate').text('save'); //change button text
          $('#btnUpdate').attr('disabled',false); //set button enable
      }
  });
}
function select() {
  $.ajax({
    type:'post',
    dataType:'json',
    url:'<?= base_url().'admin/alumni/getJurusan'?>',
    success:function (data) {
        var html ='';
        var i;
        for (var i = 0; i < data.length; i++) {
          html += '<option value="'+data[i].id_jurusan+'">'+data[i].nama_jurusan+'</option>';
        }
        $('#jurusan').html(html);
        $('#jurusan2').html(html);
        $('#jurusan3').html(html);


    }
  });
}

</script>
