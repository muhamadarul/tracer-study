<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
      <p class="login-box-msg">LOGIN</p>
  </div>
  <div class="card">
    <div class="card-body login-card-body">
      <div class="login-logo">
        <a href=""><img src="<?= base_url(); ?>assets/images/mic.png" width="75px"></a><br>
        <a><b>TRACER STUDY</b></a><br>
      </div>
      <p class="login-box-msg">STMIK MIC CIKARANG</p>
      <form action="<?= base_url('admin/auth'); ?>" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Username" name="username" id="username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" id="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Sign In &nbsp;<i class="fas fa-sign-in-alt"></i></button>
          </div>
        </div>

        <hr>
        <p class="mb-1">
          <center><a>Version 1.0</a></center>
        </p>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
