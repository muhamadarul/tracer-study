<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Info Akademik</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Ubah Info Karir  &nbsp; </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <?php if (validation_errors()) : ?>
                  <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                      <?= validation_errors(); ?>
                  </div>
              <?php endif; ?>
              <?php echo form_open_multipart('admin/info_karir/ubah'); ?>
              <?= $this->session->flashdata('message');?>
              <div class="form-group">
                <label>Judul</label>
                  <input type="text" name="judul" class="form-control" placeholder="Isi Judul Info..." value="<?= $view['judul']; ?>">
              </div>
              <div class="form-group">
                <label>Isi</label>
                <textarea name="deskripsi" class="textarea" placeholder="Place some text here"><?= $view['deskripsi']; ?></textarea>
              </div>
              <div class="form-group" hidden>
                <label>User</label>
                  <input type="text" name="dipost" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?= $user['id_user'];?>">
              </div>
              <div class="form-group">
                  <label>Gambar</label>
                    <input name="foto" type="file" class="form-control" id="exampleInputFile">
              </div>

              <div class="form-group">
                  <label>Status</label>
                  <select name="status" class="form-control select2" style="width: 100%;">
                      <option value="1" selected="selected">Publish</option>
                      <option value="0">Tidak Dipublish</option>
                  </select>
              </div>
              <input type="text" name="dibuat" value="<?= date('Y-m-d H:i:s'); ?>" hidden>
              <input type="text" name="id" value="<?= $view['id']; ?>" hidden>
              <input type="text" name="foto_lama" value="<?= $view['foto']; ?>" hidden>
            <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
$(document).ready(function() {
  // Summernote
  $('.textarea').summernote();

});
</script>
