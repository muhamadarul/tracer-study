<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Kelola User  &nbsp; <i class="fas fa-user-cog"></i></h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data User  &nbsp; <i class="fas fa-user-cog"></i></h3>
              <button data-toggle="modal" data-target="#modalTambah"  class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i>&nbsp; Tambah </button>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Username</th>
                  <th>Status</th>
                  <th>Dibuat</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody id="tampildata">

                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Modal Tambah-->
<form id="tambah" method="post">
  <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Tambah Data User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <center><font color="red"><p id="pesan"></p></font></center>
            <div class="form-group">
              <label class="col-form-label">Nama :</label>
              <input type="text" class="form-control" id="nama" name="nama">
            </div>
            <div class="form-group">
              <label class="col-form-label">Username :</label>
              <input type="text" class="form-control" id="username" name="username">
            </div>
            <div class="form-group">
              <label class="col-form-label">Status :</label>
              <select class="form-control" name="status">
                <option value="1" selected>Aktif</option>
                <option value="0">Tidak Aktif</option>
              </select>
            </div>
            <div class="form-group">
              <label class="col-form-label">Password :</label>
              <input type="password" class="form-control" id="password1" name="password1">
            </div><div class="form-group">
              <label class="col-form-label">Masukan Ulang Password :</label>
              <input type="password" class="form-control" id="password2" name="password2">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
<!-- Modal Ubah-->
<form id="ubah" method="post">
  <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelmodal">Ubah User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center><font color="red"><p id="e_pesan"></p></font></center>
            <div class="form-group" hidden>
              <label class="col-form-label">Id :</label>
              <input type="text" class="form-control" id="e_user" name="e_user">
            </div>
            <div class="form-group">
              <label class="col-form-label">Nama :</label>
              <input type="text" class="form-control" id="e_nama" name="e_nama">
            </div>
            <div class="form-group">
              <label class="col-form-label">Username :</label>
              <input type="text" class="form-control" id="e_username" name="e_username" disabled>
            </div>
            <div class="form-group">
              <label class="col-form-label">Status :</label>
              <select class="form-control" name="e_status">
                <option value="1">Aktif</option>
                <option value="0">Tidak Aktif</option>
              </select>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Batal</button>
          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp; Ubah</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
   view();
});
$(function () {
  $("#example1").DataTable();
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
  });
});
$('#tambah').on('submit', function(event){
  event.preventDefault();
      $.ajax({
          type:'POST',
          url:"<?= base_url().'admin/user/tambahuser'?>",
          data:$(this).serialize(),
          dataType:'json',
          success:function(data){
            $('#pesan').html(data.pesan);
            if (data.pesan=="") {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil ditambahkan!',
                  type: 'success'
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
              view();
              erase();
            }
            }
    })
});
function edit(id) {
  $.ajax({
    type:"POST",
    data:'id='+id,
    url:'<?= base_url().'admin/user/getById'?>',
    dataType:'json',
    success:function(data) {
      document.getElementById('e_user').value=data.id_user;
      document.getElementById('e_nama').value=data.nama;
      document.getElementById('e_username').value=data.username;
      $('[name="e_status"]').val(data.status).trigger('change');
    }
  });
}
$('#ubah').on('submit', function(event){
  event.preventDefault();
      $.ajax({
          url:"<?= base_url().'admin/user/ubah'?>",
          type:"POST",
          data:$(this).serialize(),
          dataType:'json',
          success:function(data){
            $('#e_pesan').html(data.pesan);
            if (data.pesan=='') {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil diubah!',
                  type: 'success'
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
          }
          view();
        }
      })
});
function erase() {
  document.getElementById("nama").value = "";
    document.getElementById("username").value = "";
      document.getElementById("status").value = "";
       document.getElementById("password1").value = "";
       document.getElementById("password2").value = "";

}
function view() {
  $.ajax({
    type:'POST',
    url:'<?= base_url().'admin/user/view'?>',
    dataType:'json',
    async:false,
    success:function(data){
      var baris ='';
      var n='';
      var s=[];
      for(var i=0;i<data.length;i++){
        n=i+1;
        if(data[i].status == 1) {
           s[i] = '<td>Aktif</td>';
        }else{
           s[i] = '<td>Non Aktif</td>';
        }
        baris += '<tr>'+
                      '<td>'+  n +'</td>'+
                      '<td>'+ data[i].nama+'</td>'+
                      '<td>'+ data[i].username+'</td>'+
                         s[i] +
                       '<td>'+ data[i].dibuat+'</td>'+
                      '<td><a onclick="edit('+ data[i].id_user+')" data-toggle="modal" data-target="#modalEdit" class="btn btn-default btn-outline-success btn-sm"><i class="fa fa-edit"></i></a><a onclick="hapus('+ data[i].id_user+')" class="btn btn-default btn-outline-danger btn-sm"><i class="fa fa-trash"></i></a></td>'+
                 '</tr>';
      }
      $('#tampildata').html(baris);
    }
  });
}
function hapus(id) {
  Swal.fire({
      title: "Apakah anda yakin?",
      text: "data akan dihapus!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Hapus Data!'
  }).then((result) => {
      if (result.value) {
        $.ajax({
          type:'POST',
          data:'id='+id,
          url:'<?= base_url().'admin/user/hapus'?>',
          success : function() {
            Swal.fire({
                title: 'Berhasil ',
                text: 'Data berhasil dihapus!',
                type: 'success'
            });
              view();
          }
        });
      }
  });
}
</script>
