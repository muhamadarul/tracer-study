<style>
	table{
		border-collapse: collapse;
	}
	table, th, td{
		border: solid black;
	}
	/* th, td{
		padding: 10px;
	} */
	th{
		background-color: rgb(19,110,170);
		color: white;
	}
	tr:hover{
		background-color: #f5f5f5;
	}
</style>
<center>
<table border="1" width="100%">
  <tr>
    <td>
      <center>
      <img src="<?= base_url().'assets/images/mic.png'?>" width="150px">
      </center>
    </td>
    <td>
      <center><h1>TRACER STUDY STMIK MIC CIKARANG</h1></center>
      <center><p>Harco Teknik, Blok H 1-5, Kawasan Industri Jababeka, Pasir Gombong, Kec. Cikarang Utara, Bekasi, Jawa Barat 17530</p></center>
      <center><p>Email : info@mic.ac.id &nbsp; Website : mic.ac.id&nbsp; Telepon : (021) 8936726</p></center>
    </td>
  </tr>
</table>
</center>
<hr/>
<h3><center>Data Saran</center></h3>
<div class="table-responsive">
	<table border="1px"  width="100%" style="text-align:center;">
	<tr>
		<th>No</th>
		<th>Alumni</th>
		<th>Saran</th>
	</tr>
  <?php if(is_array($saran) || is_object($saran)):?>
	<?php $no = 0; foreach($saran as $row) : $no++ ?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $row['nama']; ?></td>
			<td><?= $row['saran']; ?></td>
		</tr>
	<?php endforeach; ?>
  <?php endif; ?>
</table>
</div>
