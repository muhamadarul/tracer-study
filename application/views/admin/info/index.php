<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Info Akademik</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Info Akademik  &nbsp; </h3>
              <a href="<?= base_url('admin/info_akademik/tambah');?>"  class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i>&nbsp; Tambah </a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Judul</th>
                  <th>Deskripsi</th>
                  <th>Gambar</th>
                  <th>Status</th>
                  <th>Dibuat</th>
                  <th>Diperbarui</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php $no = 0; foreach ($view as $row):  $no++ ?>
                    <tr>
                      <td><?= $no;?></td>
                      <td><?= $row['judul'];?></td>
                      <td><?= substr($row['deskripsi'], 0, 100) . '......'; ?></td>
                      <td><img src="<?= base_url(); ?>assets/images/info/<?= $row['foto']; ?>" alt="" width="60px" title="<?= $row['foto']; ?>"></td>
                      <td><?php if ($row['verifikasi'] == 1) : ?>
                          <span class="badge badge-success">Published</span>
                          <?php else : ?>
                          <span class="badge badge-danger">Not Published</span>
                          <?php endif; ?>
                      </td>
                      <td><?= $row['nama'];?></td>
                      <td><p class="date"><?= date('d/m/Y', strtotime($row['diperbarui'])); ?> </p></td>
                      <td>
                        <a href="<?= base_url('admin/info_akademik/ubah/'); ?><?= $row['id']; ?>" class="btn-circle btn-success btn-xs"><i class="fas fa-edit"></i></a>
                        <a href="<?= base_url('admin/info_akademik/hapus/'); ?><?= $row['id']; ?>" class="btn-circle btn-danger btn-xs tombol-hapus"><i class="fas fa-trash"></i></a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
$(document).ready(function() {
  // Summernote
  $('.textarea').summernote();
   view();
   select();
});
$(function () {
  $("#example1").DataTable();
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
  });
});
