<style>
	table{
		border-collapse: collapse;
	}
	table, th, td{
		border: solid black;
	}
	/* th, td{
		padding: 10px;
	} */
	th{
		background-color: rgb(19,110,170);
		color: white;
	}
	tr:hover{
		background-color: #f5f5f5;
	}
</style>
<center>
<table border="1" width="100%">
  <tr>
    <td>
      <center>
      <img src="<?= base_url().'assets/images/mic.png'?>" width="150px">
      </center>
    </td>
    <td>
      <center><h1>TRACER STUDY STMIK MIC CIKARANG</h1></center>
      <center><p>Harco Teknik, Blok H 1-5, Kawasan Industri Jababeka, Pasir Gombong, Kec. Cikarang Utara, Bekasi, Jawa Barat 17530</p></center>
      <center><p>Email : info@mic.ac.id &nbsp; Website : mic.ac.id&nbsp; Telepon : (021) 8936726</p></center>
    </td>
  </tr>
</table>
</center>
<hr/>
<h3><center>Data Alumni</center></h3>
<div class="table-responsive">
	<table border="1px"  width="100%" style="text-align:center;">
	<tr>
		<th>No</th>
		<th>Nama</th>
		<th>Tempat Lahir</th>
  	<th>Tgl Lahir</th>
		<th>JK</th>
    <th>Agama</th>
    <th>Alamat</th>
    <th>Email</th>
    <th>No Tlp</th>
    <th>Tahun Lulus</th>
    <th>Jurusan</th>
	</tr>
  <?php if(is_array($alumni) || is_object($alumni)):?>
	<?php $no = 0; foreach($alumni as $row) : $no++ ?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $row['nama']; ?></td>
			<td><?= $row['tmp_lahir']; ?></td>
			<td><?= $row['tgl_lahir']; ?></td>
			<td><?= $row['jk']; ?></td>
			<td><?= $row['agama']; ?></td>
      <td><?= $row['alamat']; ?></td>
      <td><?= $row['email']; ?></td>
      <td><?= $row['tlp']; ?></td>
      <td><?= $row['thn_lulus']; ?></td>
      <td><?= $row['nama_jurusan']; ?></td>
		</tr>
	<?php endforeach; ?>
  <?php endif; ?>
</table>
</div>
