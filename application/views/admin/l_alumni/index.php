<style>
  .ui-datepicker-calendar{
    display: none;
  }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Laporan Data Alumni</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
  <div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
  <div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Laporan Alumni &nbsp; </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-4">
                  <form method="post" action="<?= base_url('admin/laporan_alumni/cetak');?>">
                    <div class="form-group">
                      <label>Filter Jurusan</label>
                      <select name="jurusan" id="jurusan" class="form-control" width="40px">
                          <option value="">Pilih</option>
                          <?php foreach ($jurusan as $row): ?>
                          <option value="<?= $row['id_jurusan'];?>"><?= $row['nama_jurusan'];?></option>
                          <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Tahun Lulus</label>
                      <input name="thn_lulus" value="" class="form-control date-own" type="text">
                    </div>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-download"></i>&nbsp; Cetak</button>
                  </form>
                  <br>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="<?= base_url(); ?>assets/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

$('.date-own').datepicker({
  minViewMode:2,
  format:'yyyy'
});

</script>
