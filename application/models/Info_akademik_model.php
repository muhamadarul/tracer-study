<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Info_akademik_model extends CI_Model
{
  private $_table= 'info_akademik';
  public $id;
  public $foto = 'default.jpg';

      public function view()
      {
        $this->db->select('*, info_akademik.dibuat as diperbarui, info_akademik.status as verifikasi ');
        $this->db->from('info_akademik');
        $this->db->join('m_user', 'info_akademik.dipost = m_user.id_user');
        $query = $this->db->escape($this->db->get());
        return $query->result_array();

      }
      public function NewInfo()
      {
        $this->db->select('*, info_akademik.dibuat as diperbarui, info_akademik.status as verifikasi ');
        $this->db->from('info_akademik');
        $this->db->join('m_user', 'info_akademik.dipost = m_user.id_user');
        $this->db->limit(1);
        $this->db->order_by('id','ASC');
        $this->db->where('info_akademik.status',1);
        $query = $this->db->escape($this->db->get());
        return $query->result_array();
      }
      function get_list($limit, $start)
      {
        $this->db->select('*, info_akademik.dibuat as diperbarui, info_akademik.status as verifikasi ');
        $this->db->join('m_user', 'info_akademik.dipost = m_user.id_user');
        $this->db->order_by('id','DESC');
        $this->db->where('info_akademik.status',1);
        $query = $this->db->get('info_akademik', $limit, $start);
        return $query;
      }
      public function viewWithLimit()
      {
        $this->db->select('*, info_akademik.dibuat as diperbarui, info_akademik.status as verifikasi ');
        $this->db->from('info_akademik');
        $this->db->join('m_user', 'info_akademik.dipost = m_user.id_user');
        $this->db->order_by('id','DESC');
        $this->db->where('info_akademik.status',1);
        $query = $this->db->escape($this->db->get());
        return $query->result_array();
      }
      public function getById($id)
      {
        $data = $this->db->escape($this->db->get_where('info_akademik',array('id' => $id)));
        return $data->row_array();
      }

      public function tambah($gambar)
      {
        $post = $this->input->post();
        $this->id = "";
        $this->judul= $post["judul"];
        $this->deskripsi = $post["deskripsi"];
        $this->dipost = $post["dipost"];
        $this->status = $post["status"];
        $this->foto = $gambar;
        $this->db->insert($this->_table, $this);
      }

    public function hapus($id)
    {
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("id" => $id));
    }

    private function _deleteImage($id)
    {
        $foto = $this->db->get_where($this->_table,["id" => $id])->row();
        if($foto->foto != "default.jpg") {
            $filename = explode(".", $foto->foto)[0];
            return array_map("unlink", glob(FCPATH . "assets/images/info/$filename.*"));
        }
    }

    public function ubah($id,$gambar)
    {
      $post = $this->input->post();
      $this->id = $post["id"];
      $this->judul= $post["judul"];
      $this->deskripsi = $post["deskripsi"];
      $this->dipost = $post["dipost"];
      $this->status = $post["status"];
      $this->dibuat = $post["dibuat"];
      $this->foto = $gambar;
      $this->db->update($this->_table, $this, array('id' => $post["id"]));
    }
    public function getDetail($id)
    {
      $this->db->select('*, info_akademik.dibuat as diperbarui, info_akademik.status as verifikasi ');
      $this->db->from('info_akademik');
      $this->db->join('m_user', 'info_akademik.dipost = m_user.id_user');
      $this->db->where('id', $id);
      $this->db->where('info_akademik.status',1);
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
}
