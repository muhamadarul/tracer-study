<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat_pendidikan_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('riwayat_pendidikan');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('riwayat_pendidikan',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('riwayat_pendidikan', array("id_rpen" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('riwayat_pendidikan', array('id_rpen' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('riwayat_pendidikan', $data, array('id_rpen' => $id));
  }

}
