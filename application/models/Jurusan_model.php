<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Jurusan_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('m_jurusan');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('m_jurusan',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('m_jurusan', array("id_jurusan" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('m_jurusan', array('id_jurusan' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('m_jurusan', $data, array('id_jurusan' => $id));
  }

}
