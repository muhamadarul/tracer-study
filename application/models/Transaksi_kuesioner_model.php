<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi_kuesioner_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('t_kuesioner');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewTdetail()
  {
    $this->db->select('*');
    $this->db->from('t_kuesioner_detail');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }

  public function viewByUser($alumni)
  {
    $this->db->select('*');
    $this->db->from('t_kuesioner');
    $this->db->where('alumni',$alumni);
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('t_kuesioner',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('t_kuesioner', array("id_tks" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('t_kuesioner', array('id_tks' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('t_kuesioner', $data, array('id_tks' => $id));
  }
  public function checkExist($alumni)
  {
      return $this->db->get_where('t_kuesioner',array('alumni'=>$alumni))->num_rows();
  }
  public function getIdbyUser($alumni)
  {
    $query = $this->db->escape($this->db->get_where('t_kuesioner', array('alumni' => $alumni)));
    return $query->row_array();
  }
  public function simpanDetail($result)
  {
    return $this->db->insert_batch('t_kuesioner_detail', $result);
  }
  public function validasi($alumni)
  {
    $data =[
      'validasi' => 1,
      'diisi' => date('Y-m-d'),
    ];
    return $this->db->update('t_kuesioner', $data, array('alumni' => $alumni));
  }
  public function cekValidasi($alumni)
  {
    $query = $this->db->escape($this->db->get_where('t_kuesioner', array('alumni' => $alumni)));
    return $query->row_array();
  }
  public function cekSave($kuesioner)
  {
      return $this->db->get_where('t_kuesioner_detail',array('pertanyaan'=>$kuesioner))->num_rows();
  }
  public function graphGaji()
  {
    $this->db->select('*, m_jawaban.jawaban as jwbn, COUNT(t_kuesioner_detail.jawaban) AS jumlah');
    $this->db->from('t_kuesioner_detail');
    $this->db->join('m_jawaban', 'm_jawaban.id_jawaban = t_kuesioner_detail.jawaban');
    $this->db->where('pertanyaan',10);
    $this->db->group_by('t_kuesioner_detail.jawaban');
    $query = $this->db->escape($this->db->get());
    return $query->result();
  }
}
