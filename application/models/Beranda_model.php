<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Beranda_model extends CI_Model
{
  public function hitungalumni()
  {
      $query = $this->db->get('alumni');
      if ($query->num_rows() > 0) {
          return $query->num_rows();
      } else {
          return 0;
      }
  }
  public function hitunguser()
  {
      $query = $this->db->get('m_user');
      if ($query->num_rows() > 0) {
          return $query->num_rows();
      } else {
          return 0;
      }
  }
}
