<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat_kerja_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('riwayat_kerja');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('riwayat_kerja',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('riwayat_kerja', array("id_rk" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('riwayat_kerja', array('id_rk' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('riwayat_kerja', $data, array('id_rk' => $id));
  }

}
