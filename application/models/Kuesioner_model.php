<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kuesioner_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('m_kuesioner');
    $this->db->order_by('urutan','ASC');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }

  public function getKuesioner()
  {
    $this->db->select('id_kuesioner');
    $this->db->from('m_kuesioner');
    $this->db->order_by('urutan','ASC');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewAll()
  {
    $this->db->select('*, m_kuesioner.kuesioner as pertanyaan');
    $this->db->from('m_kuesioner');
    $this->db->join('m_jawaban', 'm_jawaban.kuesioner = m_kuesioner.id_kuesioner');
    $this->db->order_by('urutan','ASC');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('m_kuesioner',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('m_kuesioner', array("id_kuesioner" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('m_kuesioner', array('id_kuesioner' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('m_kuesioner', $data, array('id_kuesioner' => $id));
  }



}
