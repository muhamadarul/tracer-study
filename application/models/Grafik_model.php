<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Grafik_model extends CI_Model
{
  public function grafikIsi()
  {
    $jumlahisi = $this->db->get('t_kuesioner');
    if ($jumlahisi->num_rows() > 0) {
        $resultisi = $jumlahisi->num_rows();
    } else {
        $resultisi = 0;
    }
    $jumlahalumni = $this->db->get('alumni');
    if ($jumlahalumni->num_rows() > 0) {
        $resultalumni = $jumlahalumni->num_rows();
    } else {
        $resultalumni = 0;
    }
    $tidakMengisi = $resultalumni - $resultisi;
    $resultTidakMengisi = ($tidakMengisi/$resultalumni)*100;
    $resultMengisi = ($resultisi/$resultalumni)*100;
    $data = [
      $resultMengisi,$resultTidakMengisi
    ];
    return $data;
  }

  public function grafikAktivitas()
  {
    $jumlahisi = $this->db->get('t_kuesioner');
    if ($jumlahisi->num_rows() > 0) {
        $resultisi = $jumlahisi->num_rows();
    } else {
        $resultisi = 0;
    }
    $a =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 1)));
    if ($a->num_rows() > 0) {
        $result1 = $a->num_rows();
    } else {
        $result1 = 0;
    }
    $b =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 2)));
    if ($b->num_rows() > 0) {
        $result2 = $b->num_rows();
    } else {
        $result2 = 0;
    }
    $c =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 3)));
    if ($c->num_rows() > 0) {
        $result3 = $c->num_rows();
    } else {
        $result3 = 0;
    }
    $d =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 4)));
    if ($d->num_rows() > 0) {
        $result4 = $d->num_rows();
    } else {
        $result4 = 0;
    }
    $e =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 5)));
    if ($e->num_rows() > 0) {
        $result5 = $e->num_rows();
    } else {
        $result5 = 0;
    }
    $bekerja = ($result1/$resultisi)*100;
    $tidakbekerja = ($result2/$resultisi)*100;
    $belumbekerja = ($result3/$resultisi)*100;
    $wirausaha = ($result4/$resultisi)*100;
    $studi = ($result5/$resultisi)*100;
    $data = [$bekerja,$tidakbekerja,$belumbekerja,$wirausaha,$studi];
    return $data;
  }


  public function grafikPendapatan()
  {
    $a =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 41)));
    if ($a->num_rows() > 0) {
        $result1 = $a->num_rows();
    } else {
        $result1 = 0;
    }
    $b =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 42)));
    if ($b->num_rows() > 0) {
        $result2 = $b->num_rows();
    } else {
        $result2 = 0;
    }
    $c =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 43)));
    if ($c->num_rows() > 0) {
        $result3 = $c->num_rows();
    } else {
        $result3 = 0;
    }
    $d =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 44)));
    if ($d->num_rows() > 0) {
        $result4 = $d->num_rows();
    } else {
        $result4 = 0;
    }
    $e =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 45)));
    if ($e->num_rows() > 0) {
        $result5 = $e->num_rows();
    } else {
        $result5 = 0;
    }
    $f =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 46)));
    if ($f->num_rows() > 0) {
        $result6 = $f->num_rows();
    } else {
        $result6 = 0;
    }
    $g =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 47)));
    if ($g->num_rows() > 0) {
        $result7 = $g->num_rows();
    } else {
        $result7 = 0;
    }
    $h =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 48)));
    if ($h->num_rows() > 0) {
        $result8 = $h->num_rows();
    } else {
        $result8 = 0;
    }
    $i =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 49)));
    if ($i->num_rows() > 0) {
        $result9 = $i->num_rows();
    } else {
        $result9 = 0;
    }
    $j =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 50)));
    if ($j->num_rows() > 0) {
        $result10 = $j->num_rows();
    } else {
        $result10 = 0;
    }
    $data = [$result1,$result2,$result3,$result4,$result5,$result6,$result7,$result8,$result9,$result10];
    return $data;
  }

  public function grafikKerjaUsaha()
  {
    $jumlahisi = $this->db->get('t_kuesioner');
    if ($jumlahisi->num_rows() > 0) {
        $resultisi = $jumlahisi->num_rows();
    } else {
        $resultisi = 0;
    }
    $a =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 10)));
    if ($a->num_rows() > 0) {
        $result1 = $a->num_rows();
    } else {
        $result1 = 0;
    }
    $b =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 11)));
    if ($b->num_rows() > 0) {
        $result2 = $b->num_rows();
    } else {
        $result2 = 0;
    }
    $c =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 12)));
    if ($c->num_rows() > 0) {
        $result3 = $c->num_rows();
    } else {
        $result3 = 0;
    }
    $sesuai = ($result1/$resultisi)*100;
    $tidaksesuai = ($result2/$resultisi)*100;
    $wirausaha = ($result3/$resultisi)*100;
    $data = [$sesuai,$tidaksesuai,$wirausaha];
    return $data;
  }

  public function grafikKondisiKerja()
  {
    $jumlahisi = $this->db->get('t_kuesioner');
    if ($jumlahisi->num_rows() > 0) {
        $resultisi = $jumlahisi->num_rows();
    } else {
        $resultisi = 0;
    }
    $a =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 8)));
    if ($a->num_rows() > 0) {
        $result1 = $a->num_rows();
    } else {
        $result1 = 0;
    }
    $b =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 9)));
    if ($b->num_rows() > 0) {
        $result2 = $b->num_rows();
    } else {
        $result2 = 0;
    }
    $paruh = ($result1/$resultisi)*100;
    $penuh = ($result2/$resultisi)*100;
    $data = [$paruh,$penuh];
    return $data;
  }

  public function grafikKorelasi()
  {
    $jumlahisi = $this->db->get('t_kuesioner');
    if ($jumlahisi->num_rows() > 0) {
        $resultisi = $jumlahisi->num_rows();
    } else {
        $resultisi = 0;
    }
    $a =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 27)));
    if ($a->num_rows() > 0) {
        $result1 = $a->num_rows();
    } else {
        $result1 = 0;
    }
    $b =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 28)));
    if ($b->num_rows() > 0) {
        $result2 = $b->num_rows();
    } else {
        $result2 = 0;
    }
    $c =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 29)));
    if ($c->num_rows() > 0) {
        $result3 = $c->num_rows();
    } else {
        $result3 = 0;
    }
    $d =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 30)));
    if ($d->num_rows() > 0) {
        $result4 = $d->num_rows();
    } else {
        $result4 = 0;
    }
    $e =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 31)));
    if ($e->num_rows() > 0) {
        $result5 = $e->num_rows();
    } else {
        $result5 = 0;
    }
    // $ss = ($result1/$resultisi)*100;
    // $s = ($result2/$resultisi)*100;
    // $sd = ($result3/$resultisi)*100;
    // $ts = ($result4/$resultisi)*100;
    // $sts = ($result5/$resultisi)*100;
    // $data = [$ss,$s,$sd,$ts,$sts];
    $data = [$result1,$result2,$result3,$result4,$result5];
    return $data;
  }

  public function grafikWaktuTunggu()
  {
    $jumlahisi = $this->db->get('t_kuesioner');
    if ($jumlahisi->num_rows() > 0) {
        $resultisi = $jumlahisi->num_rows();
    } else {
        $resultisi = 0;
    }
    $a =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 52)));
    if ($a->num_rows() > 0) {
        $result1 = $a->num_rows();
    } else {
        $result1 = 0;
    }
    $b =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 53)));
    if ($b->num_rows() > 0) {
        $result2 = $b->num_rows();
    } else {
        $result2 = 0;
    }
    $c =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 54)));
    if ($c->num_rows() > 0) {
        $result3 = $c->num_rows();
    } else {
        $result3 = 0;
    }
    $d =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 55)));
    if ($d->num_rows() > 0) {
        $result4 = $d->num_rows();
    } else {
        $result4 = 0;
    }
    $e =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 56)));
    if ($e->num_rows() > 0) {
        $result5 = $e->num_rows();
    } else {
        $result5 = 0;
    }
    $f =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 57)));
    if ($f->num_rows() > 0) {
        $result6 = $f->num_rows();
    } else {
        $result6 = 0;
    }
    $data = [$result1,$result2,$result3,$result4,$result5,$result6];
    return $data;
  }

  public function grafikTingkatPendidikan()
  {
    $jumlahisi = $this->db->get('t_kuesioner');
    if ($jumlahisi->num_rows() > 0) {
        $resultisi = $jumlahisi->num_rows();
    } else {
        $resultisi = 0;
    }
    $a =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 37)));
    if ($a->num_rows() > 0) {
        $result1 = $a->num_rows();
    } else {
        $result1 = 0;
    }
    $b =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 38)));
    if ($b->num_rows() > 0) {
        $result2 = $b->num_rows();
    } else {
        $result2 = 0;
    }
    $c =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 39)));
    if ($c->num_rows() > 0) {
        $result3 = $c->num_rows();
    } else {
        $result3 = 0;
    }
    $d =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 40)));
    if ($d->num_rows() > 0) {
        $result4 = $d->num_rows();
    } else {
        $result4 = 0;
    }
    $data = [$result1,$result2,$result3,$result4];
    return $data;
  }

  public function grafikDapatKerja()
  {
    $a =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 13)));
    if ($a->num_rows() > 0) {
        $result1 = $a->num_rows();
    } else {
        $result1 = 0;
    }
    $b =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 14)));
    if ($b->num_rows() > 0) {
        $result2 = $b->num_rows();
    } else {
        $result2 = 0;
    }
    $c =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 15)));
    if ($c->num_rows() > 0) {
        $result3 = $c->num_rows();
    } else {
        $result3 = 0;
    }
    $d =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 16)));
    if ($d->num_rows() > 0) {
        $result4 = $d->num_rows();
    } else {
        $result4 = 0;
    }
    $e =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 17)));
    if ($e->num_rows() > 0) {
        $result5 = $e->num_rows();
    } else {
        $result5 = 0;
    }
    $f =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 18)));
    if ($f->num_rows() > 0) {
        $result6 = $f->num_rows();
    } else {
        $result6 = 0;
    }
    $g =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 19)));
    if ($g->num_rows() > 0) {
        $result7 = $g->num_rows();
    } else {
        $result7 = 0;
    }
    $h =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 20)));
    if ($h->num_rows() > 0) {
        $result8 = $h->num_rows();
    } else {
        $result8 = 0;
    }
    $i =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 21)));
    if ($i->num_rows() > 0) {
        $result9 = $i->num_rows();
    } else {
        $result9 = 0;
    }
    $j =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 22)));
    if ($j->num_rows() > 0) {
        $result10 = $j->num_rows();
    } else {
        $result10 = 0;
    }
    $k =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 23)));
    if ($k->num_rows() > 0) {
        $result11 = $k->num_rows();
    } else {
        $result11 = 0;
    }
    $l =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 24)));
    if ($l->num_rows() > 0) {
        $result12 = $l->num_rows();
    } else {
        $result12 = 0;
    }
    $m =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 25)));
    if ($m->num_rows() > 0) {
        $result13 = $m->num_rows();
    } else {
        $result13 = 0;
    }
    $n =  $this->db->escape($this->db->get_where('t_kuesioner_detail', array('jawaban' => 26)));
    if ($n->num_rows() > 0) {
        $result14 = $n->num_rows();
    } else {
        $result14 = 0;
    }
    $data = [$result1,$result2,$result3,$result4,$result5,$result6,$result7,$result8,$result9,$result10,$result11,$result12,$result13,$result14];
    return $data;
  }


}
