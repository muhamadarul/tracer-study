<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pertanyaan_langsung_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('ks_langsung');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('ks_langsung',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('ks_langsung', array("id_ksl" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('ks_langsung', array('id_ksl' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('ks_langsung', $data, array('id_ksl' => $id));
  }

}
