<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
  public function view()
  {
    $this->db->select('username,status,nama,id_user,dibuat');
    $this->db->from('m_user');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('m_user',$data);
  }
  public function cekUsername($username)
  {
    $this->db->select('username');
    $this->db->from('m_user');
    $this->db->where('username',$username);
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function hapus($id)
  {
     return $this->db->delete('m_user', array("id_user" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('m_user', array('id_user' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('m_user', $data, array('id_user' => $id));
  }
  
}
