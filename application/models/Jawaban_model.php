<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Jawaban_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('m_jawaban');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function viewall()
  {
    $this->db->select('*');
    $this->db->from('m_jawaban');
    $query = $this->db->escape($this->db->get());
    return $query->row_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('m_jawaban',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('m_jawaban', array("id_jawaban" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('m_jawaban', array('id_jawaban' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('m_jawaban', $data, array('id_jawaban' => $id));
  }
  public function viewGaji()
  {
    $this->db->select('*');
    $this->db->from('m_jawaban');
    $this->db->where('kuesioner',10);
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }

}
