<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pertanyaan_banyak_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('ks_bpilihan');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('ks_bpilihan',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('ks_bpilihan', array("id_ksbp" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('ks_bpilihan', array('id_ksbp' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('ks_bpilihan', $data, array('id_ksbp' => $id));
  }

}
