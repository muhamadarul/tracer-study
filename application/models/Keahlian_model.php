<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Keahlian_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('keahlian');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('keahlian',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('keahlian', array("id_keahlian" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('keahlian', array('id_keahlian' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('keahlian', $data, array('id_keahlian' => $id));
  }

}
