<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Jawaban_pilihan_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('j_ksp');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('j_ksp',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('j_ksp', array("id_jksp" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('j_ksp', array('id_jksp' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('j_ksp', $data, array('id_jksp' => $id));
  }

}
