<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Alumni_model extends CI_Model
{
  var $table = 'alumni';
      var $column_order = array(null, 'nama','tmp_lahir','tgl_lahir','email','jurusan','thn_lulus'); //set column field database for datatable orderable
      var $column_search = array( 'nama','tmp_lahir','tgl_lahir','email','jurusan','thn_lulus'); //set column field database for datatable searchable
      var $order = array('id_alumni' => 'asc'); // default order

      public function __construct()
      {
          parent::__construct();
          $this->load->database();
      }

      private function _get_datatables_query()
      {

          $this->db->from($this->table);

          $i = 0;

          foreach ($this->column_search as $item) // loop column
          {
              if($_POST['search']['value']) // if datatable send POST for search
              {

                  if($i===0) // first loop
                  {
                      $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                      $this->db->like($item, $_POST['search']['value']);
                  }
                  else
                  {
                      $this->db->or_like($item, $_POST['search']['value']);
                  }

                  if(count($this->column_search) - 1 == $i) //last loop
                      $this->db->group_end(); //close bracket
              }
              $i++;
          }

          if(isset($_POST['order'])) // here order processing
          {
              $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
          }
          else if(isset($this->order))
          {
              $order = $this->order;
              $this->db->order_by(key($order), $order[key($order)]);
          }
      }

      function get_datatables()
      {
          $this->_get_datatables_query();
          if(isset($_POST["length"]) && $_POST["length"] != -1)
          $this->db->limit($_POST['length'], $_POST['start']);
          $query = $this->db->get();
          return $query->result();
      }

      function count_filtered()
      {
          $this->_get_datatables_query();
          $query = $this->db->get();
          return $query->num_rows();
      }

      public function count_all()
      {
          $this->db->from($this->table);
          return $this->db->count_all_results();
      }
      public function tambah($data)
      {
        return $this->db->insert('alumni',$data);
      }
      public function hapus($id)
      {
        $this->_deleteImage($id);
         $this->db->delete('alumni', array("id_alumni" => $id));
      }
      public function getById($id)
      {
        $query = $this->db->escape($this->db->get_where('alumni', array('id_alumni' => $id)));
        return $query->row_array();
      }
      public function ubah($data,$id)
      {
        $this->db->update('alumni', $data, array('id_alumni' => $id));
      }
      private function _deleteImage($id)
      {
          $admin = $this->db->get_where('alumni',["id_alumni" => $id])->row();
          if ($admin->foto != "default.jpg") {
              $filename = explode(".", $admin->foto)[0];
              return array_map('unlink', glob(FCPATH . "assets/images/alumni/$filename.*"));
            }
      }
      public function listemail()
      {
        $this->db->select('email,nama');
        $this->db->from('alumni');
        $query = $this->db->escape($this->db->get());
        return $query->result_array();
      }

      function getRows($params = array()){
        $this->db->select('*');
        $this->db->from('alumni');

        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach($params['conditions'] as $key => $value){
                $this->db->where($key,$value);
            }
        }

        if(array_key_exists("id_alumni",$params)){
            $this->db->where('id_alumni',$params['id_alumni']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $this->db->count_all_results();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->row_array():false;
            }else{
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():false;
            }
        }

        //return fetched data
        return $result;
    }
    public function viewAll()
    {
      $this->db->select('*, keahlian.alumni, riwayat_kerja.alumni , riwayat_pendidikan.alumni  ');
      $this->db->from('alumni');
      $this->db->join('m_jurusan', 'm_jurusan.id_jurusan = alumni.jurusan');
      $this->db->join('keahlian', 'keahlian.alumni = alumni.id_alumni');
      $this->db->join('riwayat_kerja', 'riwayat_kerja.alumni = alumni.id_alumni');
      $this->db->join('riwayat_pendidikan', 'riwayat_pendidikan.alumni = alumni.id_alumni');
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function viewWithJurusan($id)
    {
      $this->db->select('*');
      $this->db->from('alumni');
      $this->db->join('m_jurusan', 'm_jurusan.id_jurusan = alumni.jurusan');
      $this->db->where('id_alumni', $id);
      $query = $this->db->escape($this->db->get());
      return $query->row_array();
    }
    public function cetakFilter($jurusan,$thn_lulus)
    {
      $this->db->select('*');
      $this->db->from('alumni');
      $this->db->join('m_jurusan', 'm_jurusan.id_jurusan = alumni.jurusan');
      $this->db->where('jurusan', $jurusan);
      $this->db->where('thn_lulus', $thn_lulus);
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function cetakFilterbyJurusan($jurusan)
    {
      $this->db->select('*');
      $this->db->from('alumni');
      $this->db->join('m_jurusan', 'm_jurusan.id_jurusan = alumni.jurusan');
      $this->db->where('jurusan', $jurusan);
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function cetakFilterbyTahun($thn_lulus)
    {
      $this->db->select('*');
      $this->db->from('alumni');
      $this->db->join('m_jurusan', 'm_jurusan.id_jurusan = alumni.jurusan');
      $this->db->where('thn_lulus', $thn_lulus);
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function cetakAll()
    {
      $this->db->select('*');
      $this->db->from('alumni');
      $this->db->join('m_jurusan', 'm_jurusan.id_jurusan = alumni.jurusan');
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }


}
