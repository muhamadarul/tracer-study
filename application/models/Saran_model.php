<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Saran_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('m_saran');
    $this->db->join('alumni', 'm_saran.alumni = alumni.id_alumni');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('m_saran',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('m_saran', array("id" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('m_saran', array('id' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('m_saran', $data, array('id' => $id));
  }
  public function checkExist($alumni)
  {
      return $this->db->get_where('m_saran',array('alumni'=>$alumni))->num_rows();
  }
  public function cetakAll()
  {
    $this->db->select('*');
    $this->db->from('m_saran');
    $this->db->join('alumni', 'alumni.id_alumni = m_saran.alumni');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }

}
