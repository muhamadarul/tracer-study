<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pertanyaan_pilihan_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('ks_pilihan');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('ks_pilihan',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('ks_pilihan', array("id_ksp" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('ks_pilihan', array('id_ksp' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('ks_pilihan', $data, array('id_ksp' => $id));
  }
}
