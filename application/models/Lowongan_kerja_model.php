<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Lowongan_kerja_model extends CI_Model
{
  public function view()
  {
    $this->db->select('*');
    $this->db->from('m_lowongan');
    $query = $this->db->escape($this->db->get());
    return $query->result_array();
  }
  public function tambah($data)
  {
    return $this->db->insert('m_lowongan',$data);
  }
  public function hapus($id)
  {
     return $this->db->delete('m_lowongan', array("id_lowongan" => $id));
  }
  public function getById($id)
  {
    $query = $this->db->escape($this->db->get_where('m_lowongan', array('id_lowongan' => $id)));
    return $query->row_array();
  }
  public function ubah($id,$data)
  {
    return $this->db->update('m_lowongan', $data, array('id_lowongan' => $id));
  }

}
