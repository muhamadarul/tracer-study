<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{

    public function login($username, $password)
    {
        $user = $this->db->get_where('m_user', ['username' => $username])->row_array();
        return $user;
    }

    public function success_login()
    {
        $user = $this->db->get_where('m_user', ['username' => $this->session->userdata('username')])->row_array();
        return $user;
    }
    public function login_alumni($email, $password)
    {
        $user = $this->db->get_where('alumni', ['email' => $email])->row_array();
        return $user;
    }

    public function success_login_alumni()
    {
        $user = $this->db->get_where('alumni', ['email' => $this->session->userdata('email')])->row_array();
        return $user;
    }

    // public function cek_login_alumni($email, $password)
    // {
    //     $user = $this->db->get_where('alumni', ['email' => $email])->row_array();
    //     return $user;
    // }
}
