<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
		{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->model('Auth_model');
			$this->load->helper('url');
		}
	public function index()
	{
		$this->form_validation->set_rules('email', 'email', 'required', [
			'required' => 'Email tidak boleh kosong!'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|trim', [
			'required' => 'Password tidak boleh kosong'
		]);

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Login Alumni';
			$this->load->view('templates/front_header',$data);
	    $this->load->view('frontend/login/index');
	    $this->load->view('templates/front_footer');
		} else {
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$user = $this->Auth_model->login_alumni($email, $password);

			if ($user) {
					if (password_verify($password, $user['password'])) {
						$data = [
							'email' => $user['email'],
						];
						$this->session->set_userdata($data);
						$this->session->set_flashdata('success', 'Login Berhasil');
						redirect('alumni/dashboard');
					} else {
						$this->session->set_flashdata('warning', 'Password salah, periksa kembali');
						redirect('login');
					}
			} else {
				$this->session->set_flashdata('error', 'Email belum terdaftar!');
				redirect('login');
			}
		}
	}
	public function logout()
	{

		$this->session->unset_userdata('email');
		// $this->session->unset_userdata('id_akses');
		$this->session->set_flashdata('success', 'Anda telah logout');
		redirect('login');
	}
}
