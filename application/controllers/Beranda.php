<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function __construct()
		{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->model('Info_akademik_model');
			$this->load->model('Info_karir_model');
			$this->load->helper('url');
		}
	public function index()
	{
    $data['title'] = "Beranda";
		$data['info_akademik'] = $this->Info_akademik_model->NewInfo();
		$data['info_karir'] = $this->Info_karir_model->NewInfo();
		$this->load->view('templates/front_header',$data);
    $this->load->view('frontend/beranda/index',$data);
    $this->load->view('templates/front_footer');
	}
}
