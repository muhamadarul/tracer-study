<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kuesioner extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
    $this->load->model('Kuesioner_model');
    $this->load->model('Jawaban_model');
    $this->load->model('Transaksi_kuesioner_model');
		if (!$this->session->userdata('email')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('login');
		}
	}

	private function group_by($key, $data) {
		$result = array();
		if (is_array($data) || is_object($data))
		{
			foreach($data as $val) {
				if(array_key_exists($key, $val)){
					$result[$val[$key]][] = $val;
				}else{
					$result[''][] = $val;
				}
		}

	}

		return $result;
	}
	public function index()
	{
		$data['title'] = "Kuesioner";
		$data['alumni'] = $this->Auth_model->success_login_alumni();
    $alumni = $data['alumni']['id_alumni'];
		$data['jawaban'] = $this->Kuesioner_model->viewAll();
    $data['cek'] = $this->Transaksi_kuesioner_model->checkExist($alumni);
		$data['validasi'] = $this->Transaksi_kuesioner_model->cekValidasi($alumni);
    $this->load->view('alumni/templates/header',$data);
    $this->load->view('alumni/kuesioner/index',$data);
    $this->load->view('alumni/templates/footer');
	}

  public function tambah()
  {
    $data['alumni'] = $this->Auth_model->success_login_alumni();
    $alumni = $data['alumni']['id_alumni'];
    $data = [
        'alumni' => $alumni,
     ];
    $this->Transaksi_kuesioner_model->tambah($data);
  }
	public function save()
	{
		$id['alumni'] = $this->Auth_model->success_login_alumni();
    $alumni = $id['alumni']['id_alumni'];
		$tks = $this->Transaksi_kuesioner_model->getIdbyUser($alumni);
		$soal = $this->Kuesioner_model->view();
			if (count($soal) > 0) {
				foreach ($soal as $key => $val) {
					$result[] = array(
						'tks' => $tks['id_tks'],
						'pertanyaan' => $val['id_kuesioner'],
						'jawaban' => $this->input->post($val['id_kuesioner']),
					);
				}
			}
	 $this->Transaksi_kuesioner_model->simpanDetail($result);
	 $this->Transaksi_kuesioner_model->validasi($alumni);
	 $pesan ="";
	 echo json_encode($pesan);
	}
}
