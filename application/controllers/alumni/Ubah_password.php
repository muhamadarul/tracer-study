<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ubah_password extends CI_Controller
{
  public function __construct()
  {
      parent::__construct();
      $this->load->model('Auth_model');
      if (!$this->session->userdata('email')) {
              $this->session->set_flashdata('error', 'Anda belum melakukan login!');
              redirect('login');
      }
  }
  public function index()
  {
    $data['title'] = "Ubah Password";
    $data['alumni'] = $this->Auth_model->success_login_alumni();
      $this->form_validation->set_rules('password_lama', 'Password Lama', 'required|trim', array(
          'required' => 'Password Lama harus diisi!'
      ));
      $this->form_validation->set_rules('password_baru1', 'Password Baru', 'required|trim|min_length[5]|matches[password_baru2]', array(
          'required' => 'Password baru tidak boleh kosong!',
          'matches' => 'Password tidak sesuai!',
          'min_length' => 'Password terlalu pendek!',
      ));

      $this->form_validation->set_rules('password_baru2', 'Password Baru', 'required|trim|min_length[5]|matches[password_baru1]', array(
          'required' => 'Password baru tidak boleh kosong!',
          'matches' => 'Password tidak sesuai!',
          'min_length' => 'Password terlalu pendek!',
      ));

      if ($this->form_validation->run() == false) {
        $this->load->view('alumni/templates/header',$data);
        $this->load->view('alumni/ubah_password/index');
        $this->load->view('alumni/templates/footer');
      } else {
        $password_lama = $this->input->post('password_lama');
          $password_baru = $this->input->post('password_baru1');
            if (!password_verify($password_lama, $data['alumni']['password'])) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger role="alert">Password lama salah!</div>');
            redirect('alumni/ubah_password');
            } else {
              if ($password_lama==$password_baru) {
                $this->session->set_flashdata('message',  '<div class="alert alert-danger role="alert">Password tidak boleh sama</div>');
                redirect('alumni/ubah_password');
              }else{
                $password_hash = password_hash($password_baru, PASSWORD_DEFAULT);
                $this->db->set('password', $password_hash);
                $this->db->where('id_alumni', $this->input->post('id'));
                $this->db->update('alumni');
                $this->session->set_flashdata('success', 'Password berhasil diubah!');
                redirect('alumni/dashboard');
              }
           }
      }
  }
}
