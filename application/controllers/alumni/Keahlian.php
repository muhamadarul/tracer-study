<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keahlian extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Keahlian_model');
		$this->load->model('Auth_model');
		if (!$this->session->userdata('email')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('login');
		}
	}

	public function index()
	{
		$data['title'] = "Keahlian";
		$data['alumni'] = $this->Auth_model->success_login_alumni();
    $this->load->view('alumni/templates/header',$data);
    $this->load->view('alumni/keahlian/index',$data);
    $this->load->view('alumni/templates/footer');
	}
	public function view()
	{
		$data = $this->Keahlian_model->view();
		echo json_encode($data);
	}
	public function tambah()
	{
    $nama_keahlian = $this->input->post('nama_keahlian');
    $alumni = $this->input->post('alumni');
    $persentase = $this->input->post('persentase');
		if ($nama_keahlian == '') {
		  $result['pesan'] ="Nama Keahlian Harus Diisi";
		}elseif ($persentase =='') {
		  $result['pesan'] ="Username Harus Diisi";
		}else {
			$result['pesan'] ="";
			$data = [
			    'nama_keahlian' => $nama_keahlian,
			    'alumni' => $alumni,
			    'persentase' => $persentase,
			 ];
			$this->Keahlian_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Keahlian_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
    $nama_keahlian = $this->input->post('e_nama_keahlian');
    $id = $this->input->post('id_keahlian');
    $alumni = $this->input->post('e_alumni');
    $persentase = $this->input->post('e_persentase');
    if ($nama_keahlian == '') {
		  $result['pesan'] ="Nama Keahlian Harus Diisi";
		}elseif ($persentase =='') {
		  $result['pesan'] ="Username Harus Diisi";
		}else {
			$result['pesan'] ="";
			$data = [
			    'nama_keahlian' => $nama_keahlian,
			    'alumni' => $alumni,
			    'persentase' => $persentase,
			 ];
			$this->Keahlian_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Keahlian_model->hapus($id);
	}
}
