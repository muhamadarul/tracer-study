<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat_kerja extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Riwayat_kerja_model');
		$this->load->model('Auth_model');
		if (!$this->session->userdata('email')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('login');
		}
	}

	public function index()
	{
		$data['title'] = "Riwayat Pekerjaan";
		$data['alumni'] = $this->Auth_model->success_login_alumni();
    $this->load->view('alumni/templates/header',$data);
    $this->load->view('alumni/riwayat_kerja/index',$data);
    $this->load->view('alumni/templates/footer');
	}
	public function view()
	{
		$data = $this->Riwayat_kerja_model->view();
		echo json_encode($data);
	}
	public function tambah()
	{
    $nama_perusahaan = $this->input->post('nama_perusahaan');
    $alumni = $this->input->post('alumni');
    $jabatan = $this->input->post('jabatan');
    $t_masuk = $this->input->post('t_masuk');
    $t_keluar = $this->input->post('t_keluar');

		if ($nama_perusahaan == '') {
		  $result['pesan'] ="Nama Perusahaan Harus Diisi";
		}elseif ($jabatan =='') {
		  $result['pesan'] ="Jabatan Harus Diisi";
    }elseif ($t_masuk =='') {
      $result['pesan'] ="Tahun Masuk Harus Diisi";
    }elseif ($t_keluar =='') {
      $result['pesan'] ="Tahun Keluar Harus Diisi";
		}else {
			$result['pesan'] ="";
			$data = [
			    'nama_perusahaan' => $nama_perusahaan,
			    'alumni' => $alumni,
			    'jabatan' => $jabatan,
          't_masuk' => $t_masuk,
          't_keluar' => $t_keluar,
			 ];
			$this->Riwayat_kerja_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Riwayat_kerja_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
    $id = $this->input->post('id_rk');
		$nama_perusahaan = $this->input->post('e_nama_perusahaan');
		$alumni = $this->input->post('e_alumni');
		$jabatan = $this->input->post('e_jabatan');
		$t_masuk = $this->input->post('e_t_masuk');
		$t_keluar = $this->input->post('e_t_keluar');

		if ($nama_perusahaan == '') {
			$result['pesan'] ="Nama Perusahaan Harus Diisi";
		}elseif ($jabatan =='') {
			$result['pesan'] ="Jabatan Harus Diisi";
		}elseif ($t_masuk =='') {
			$result['pesan'] ="Tahun Masuk Harus Diisi";
		}elseif ($t_keluar =='') {
			$result['pesan'] ="Tahun Keluar Harus Diisi";
		}else {
			$result['pesan'] ="";
			$data = [
					'nama_perusahaan' => $nama_perusahaan,
					'alumni' => $alumni,
					'jabatan' => $jabatan,
					't_masuk' => $t_masuk,
					't_keluar' => $t_keluar,
			 ];
			$this->Riwayat_kerja_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Riwayat_kerja_model->hapus($id);
	}
}
