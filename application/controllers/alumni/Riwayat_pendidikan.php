<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat_pendidikan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Riwayat_pendidikan_model');
		$this->load->model('Auth_model');
		if (!$this->session->userdata('email')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('login');
		}
	}

	public function index()
	{
		$data['title'] = "Riwayat pendidikan";
		$data['alumni'] = $this->Auth_model->success_login_alumni();
    $this->load->view('alumni/templates/header',$data);
    $this->load->view('alumni/riwayat_pendidikan/index',$data);
    $this->load->view('alumni/templates/footer');
	}
	public function view()
	{
		$data = $this->Riwayat_pendidikan_model->view();
		echo json_encode($data);
	}
	public function tambah()
	{
    $nama_institusi = $this->input->post('nama_institusi');
    $alumni = $this->input->post('alumni');
    $tingkat = $this->input->post('tingkat');
    $thn_masuk = $this->input->post('thn_masuk');
    $thn_lulus = $this->input->post('thn_lulus');

		if ($nama_institusi == '') {
		  $result['pesan'] ="Nama Institusi Harus Diisi";
		}elseif ($tingkat =='') {
		  $result['pesan'] ="Tingkat Harus Diisi";
    }elseif ($thn_masuk =='') {
      $result['pesan'] ="Tahun Masuk Harus Diisi";
    }elseif ($thn_lulus =='') {
      $result['pesan'] ="Tahun Lulus Harus Diisi";
		}else {
			$result['pesan'] ="";
			$data = [
			    'nama_institusi' => $nama_institusi,
			    'alumni' => $alumni,
			    'tingkat' => $tingkat,
          'thn_masuk' => $thn_masuk,
          'thn_lulus' => $thn_lulus,
			 ];
			$this->Riwayat_pendidikan_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Riwayat_pendidikan_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
    $nama_institusi = $this->input->post('e_nama_institusi');
    $alumni = $this->input->post('e_alumni');
    $tingkat = $this->input->post('e_tingkat');
    $thn_masuk = $this->input->post('e_thn_masuk');
    $thn_lulus = $this->input->post('e_thn_lulus');
    $id = $this->input->post('id_rpen');

    if ($nama_institusi == '') {
      $result['pesan'] ="Nama Institusi Harus Diisi";
    }elseif ($tingkat =='') {
      $result['pesan'] ="Tingkat Harus Diisi";
    }elseif ($thn_masuk =='') {
      $result['pesan'] ="Tahun Masuk Harus Diisi";
    }elseif ($thn_lulus =='') {
      $result['pesan'] ="Tahun Lulus Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'nama_institusi' => $nama_institusi,
          'alumni' => $alumni,
          'tingkat' => $tingkat,
          'thn_masuk' => $thn_masuk,
          'thn_lulus' => $thn_lulus,
       ];
			$this->Riwayat_pendidikan_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Riwayat_pendidikan_model->hapus($id);
	}
}
