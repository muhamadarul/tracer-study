<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Saran extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Saran_model');
		$this->load->model('Auth_model');
		if (!$this->session->userdata('email')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('login');
		}
	}

	public function index()
	{
		$data['title'] = "Saran";
		$data['alumni'] = $this->Auth_model->success_login_alumni();
    $alumni = $data['alumni']['id_alumni'];
    $data['cek'] = $this->Saran_model->checkExist($alumni);
    $this->load->view('alumni/templates/header',$data);
    $this->load->view('alumni/saran/index',$data);
    $this->load->view('alumni/templates/footer');
	}
	public function view()
	{
		$data = $this->Saran_model->view();
		echo json_encode($data);
	}
	public function tambah()
	{
    $saran = $this->input->post('saran');
    $alumni = $this->input->post('alumni');

		if ($saran == '') {
		  $result['pesan'] ="Saran Harus Diisi";
		}else {
			$result['pesan'] ="";
			$data = [
			    'saran' => $saran,
			    'alumni' => $alumni
			 ];
			$this->Saran_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Saran_model->hapus($id);
	}
}
