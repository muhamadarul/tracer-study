<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
  function __construct() {
      parent::__construct();
      $this->load->model('Auth_model');
      $this->load->model('Alumni_model');
      if (!$this->session->userdata('email')) {
              $this->session->set_flashdata('error', 'Anda belum melakukan login!');
              redirect('login');
      }
  }
	public function index()
	{
    $data['title'] = "Profil Alumni";
    $data['alumni'] = $this->Auth_model->success_login_alumni();
    $data['id']= $data['alumni']['id_alumni'];
		$this->load->view('alumni/templates/header',$data);
    $this->load->view('alumni/data_diri/index');
    $this->load->view('alumni/templates/footer');
	}
  public function ubah()
  {
    $data['alumni'] = $this->Auth_model->success_login_alumni();
    $id = $data['alumni']['id_alumni'];
    $data['view'] = $this->Alumni_model->viewWithJurusan($id);
    $data['title'] = 'Ubah Data Diri';
    $this->form_validation->set_rules('tlp', 'Telepon', 'required|trim', [
        'required' => 'No Telepon tidak boleh kosong!'
    ]);
    $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim', [
        'required' => 'Alamat tidak boleh kosong!'
    ]);

    if ($this->form_validation->run() == false) {
      $this->load->view('alumni/templates/header',$data);
      $this->load->view('alumni/data_diri/ubah',$data);
      $this->load->view('alumni/templates/footer');
    } else {
      if ((empty($_FILES['foto']['name']))) {
        $alumni = [
          'tlp' => $this->input->post('tlp'),
          'alamat' => $this->input->post('alamat'),
          'foto' => $this->input->post('foto_lama'),
        ];
        $this->db->update('alumni', $alumni, array('id_alumni' => $id));
        $this->session->set_flashdata('success', 'Data berhasil diubah!');
        redirect('alumni/profile');
      }else {
        //upload foto
        $config['upload_path']          = './assets/images/alumni';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['encrypt_name']         = TRUE; //enkripsi file name upload
        $config['overwrite']            = true;
        $config['max_size']             = 520; // 500kb
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('foto')) {
           $this->_deleteImage($id);
            $a = array('upload_data' => $this->upload->data());
            $image= $a['upload_data']['file_name'];
            $alumni = [
              'tlp' => $this->input->post('tlp'),
              'alamat' => $this->input->post('alamat'),
              'foto' => $image,
            ];
            $this->db->update('alumni', $alumni, array('id_alumni' => $id));
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('alumni/profile');
        }else {
            $error_upload = $this->upload->display_errors();
            $this->session->set_flashdata('error', $error_upload);
            redirect('alumni/profile/ubah');
        }
      }
    }

  }
  private function _deleteImage($id)
  {
      $gambar = $this->db->get_where('alumni',["id_alumni" => $id])->row();
      if($gambar->foto != "default.jpg") {
          $filename = explode(".", $gambar->foto)[0];
          return array_map("unlink", glob(FCPATH . "assets/images/alumni/$filename.*"));
      }
  }
  // public function test()
  // {
  //   $data = $this->Alumni_model->viewWithJurusan  ();
  //   var_dump($data);
  // }
}
