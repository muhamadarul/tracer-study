<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
  function __construct() {
      parent::__construct();
      $this->load->model('Auth_model');
      if (!$this->session->userdata('email')) {
              $this->session->set_flashdata('error', 'Anda belum melakukan login!');
              redirect('login');
      }
  }
	public function index()
	{
    $data['title'] = "Beranda";
    $data['alumni'] = $this->Auth_model->success_login_alumni();
		$this->load->view('alumni/templates/header',$data);
    $this->load->view('alumni/beranda/index');
    $this->load->view('alumni/templates/footer');
	}
}
