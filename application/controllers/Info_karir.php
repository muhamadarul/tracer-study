<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info_karir extends CI_Controller {

	public function __construct()
		{
			parent::__construct();
			$this->load->library('form_validation');
			$this->load->model('Info_karir_model');
			$this->load->helper('url');
      $this->load->library('pagination');

		}
	public function index()
	{
    //konfigurasi pagination
    $config['base_url'] = base_url('info_karir/index'); //site url
    $config['total_rows'] = $this->db->count_all('info_karir'); //total row
    $config['per_page'] = 6;  //show record per halaman
    $config["uri_segment"] = 3;  // uri parameter
    $choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = floor($choice);

    // Membuat Style pagination untuk BootStrap v4
    $config['first_link']       = 'First';
    $config['last_link']        = 'Last';
    $config['next_link']        = 'Next';
    $config['prev_link']        = 'Prev';
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['prev_tagl_close']  = '</span>Next</li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
    $config['first_tagl_close'] = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tagl_close']  = '</span></li>';

    $this->pagination->initialize($config);
    $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model.
    $data['view'] = $this->Info_karir_model->get_list($config["per_page"], $data['page']);
    $data['pagination'] = $this->pagination->create_links();
    $data['title'] = "Info Karir";
		$this->load->view('templates/front_header',$data);
    $this->load->view('frontend/info_karir/index',$data);
    $this->load->view('templates/front_footer');
	}
	public function detail($id)
	{
			$data['view'] = $this->Info_karir_model->getDetail($id);
			$data['title'] = "Info Karir";
			$this->load->view('templates/front_header',$data);
	    $this->load->view('frontend/info_karir/detail',$data);
	    $this->load->view('templates/front_footer');
	}
}
