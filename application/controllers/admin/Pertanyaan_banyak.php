<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pertanyaan_banyak extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Pertanyaan_banyak_model');
		$this->load->model('Auth_model');
		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('admin/auth');
		}
	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Pertanyaan Banyak Pilihan";
    $this->load->view('templates/header',$data);
    $this->load->view('admin/ks_banyak/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Pertanyaan_banyak_model->view();
		echo json_encode($data);
	}
	public function tambah()
	{
		$kd_pertanyaan	= $this->input->post('kd_pertanyaan');
    $pertanyaan = $this->input->post('pertanyaan');
		if ($kd_pertanyaan == '') {
		  $result['pesan'] ="Kode Pertanyaan Harus Diisi";
    }elseif ($pertanyaan == '') {
  	  $result['pesan'] ="Pertanyaan Harus Diisi";
		}else {
			$result['pesan'] ="";
			$data = [
			    'kd_pertanyaan' => $kd_pertanyaan,
          'pertanyaan' => $pertanyaan,
			 ];
			$this->Pertanyaan_banyak_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Pertanyaan_banyak_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_ksbp');
    $kd_pertanyaan	 = $this->input->post('e_kd_pertanyaan');
    $pertanyaan = $this->input->post('e_pertanyaan');
    if ($kd_pertanyaan == '') {
      $result['pesan'] ="Kode Pertanyaan Harus Diisi";
    }elseif ($pertanyaan == '') {
      $result['pesan'] ="Pertanyaan Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'kd_pertanyaan' => $kd_pertanyaan,
          'pertanyaan' => $pertanyaan,
       ];
			$this->Pertanyaan_banyak_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Pertanyaan_banyak_model->hapus($id);
	}
}
