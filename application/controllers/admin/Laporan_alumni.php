<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_alumni extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
    $this->load->model('Alumni_model');
		$this->load->model('Jurusan_model');

		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('admin/auth');
		}
	}

	public function index()
	{
		$data['title'] = "Laporan Alumni";
		$data['user'] = $this->Auth_model->success_login();
		$data['jurusan'] = $this->Jurusan_model->view();

    // $data['jurusan'] = $this->Jurusan_model->view();
    $this->load->view('templates/header',$data);
    $this->load->view('admin/l_alumni/index');
    $this->load->view('templates/footer');
	}
  public function cetak()
  {
    $jurusan = $_POST['jurusan'];
    $thn_lulus = $_POST['thn_lulus'];
    if (!empty($jurusan) && !empty($thn_lulus)) {
      $alumni = $this->Alumni_model->cetakFilter($jurusan,$thn_lulus);
    }elseif (!empty($jurusan) && empty($thn_lulus)) {
      $alumni = $this->Alumni_model->cetakFilterbyJurusan($jurusan);
    }elseif (empty($jurusan) && !empty($thn_lulus)) {
      $alumni = $this->Alumni_model->cetakFilterbyTahun($thn_lulus);
    }else {
      $alumni = $this->Alumni_model->cetakAll();
    }

		$this->load->library('pdf');
		$this->pdf->set_option('isRemoteEnabled', TRUE);
    $this->pdf->setPaper('A3', 'landscape');
    $this->pdf->filename = "laporan-data-alumni.pdf";
    $this->pdf->load_view('admin/l_alumni/alumni_pdf', compact('alumni'));
  }
	

}
