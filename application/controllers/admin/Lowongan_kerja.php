<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lowongan_kerja extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Lowongan_kerja_model');
		$this->load->model('Auth_model');
		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('admin/auth');
		}
	}

	public function index()
	{
		$data['title'] = "Lowongan Kerja";
		$data['user'] = $this->Auth_model->success_login();
    $this->load->view('templates/header',$data);
    $this->load->view('lowongan/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Kuesioner_model->view();
		echo json_encode($data);
	}
	public function tambah()
	{
		$kuesioner = $this->input->post('kuesioner');
		if ($kuesioner == '') {
		  $result['pesan'] ="Kuesioner Harus Diisi";
		}else {
			$result['pesan'] ="";
			$data = [
			    'kuesioner' => $kuesioner,
			 ];
			$this->Kuesioner_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Kuesioner_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_kuesioner');
		$kuesioner = $this->input->post('e_kuesioner');
		if ($kuesioner == '') {
		  $result['pesan'] ="Kuesioner Harus Diisi";
		}else {
			$result['pesan'] ="";
			$data = [
			    'kuesioner' => $kuesioner
			 ];
			$this->Kuesioner_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Kuesioner_model->hapus($id);
	}
}
