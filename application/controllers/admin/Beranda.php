<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
		$this->load->model('Beranda_model');
		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('admin/auth');
		}
	}

	public function index()
	{
		$data['title'] = "Beranda";
		$data['user'] = $this->Auth_model->success_login();
		$data['hitunguser'] = $this->Beranda_model->hitunguser();
		$data['hitungalumni'] = $this->Beranda_model->hitungalumni();
    $this->load->view('templates/header',$data);
    $this->load->view('admin/beranda/index');
    $this->load->view('templates/footer');
	}

}
