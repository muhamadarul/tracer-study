<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Info_akademik extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Info_akademik_model');
		$this->load->model('Auth_model');
		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('admin/auth');
		}
	}
	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
    $data['view'] = $this->Info_akademik_model->view();
		$data['title'] = "Info Akademik";
    $this->load->view('templates/header',$data);
    $this->load->view('admin/info/index');
    $this->load->view('templates/footer');
	}
  public function tambah()
  {
      $data['user'] = $this->Auth_model->success_login();
      $data['title'] = 'Tambah Info Akademik';
      $this->form_validation->set_rules('judul', 'Judul', 'required|trim', [
          'required' => 'Judul tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('deskripsi', 'deskripsi', 'required|trim', [
          'required' => 'deskripsi tidak boleh kosong!'
      ]);
      if ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/info/tambah', $data);
          $this->load->view('templates/footer');
      } else {
        $config['upload_path']          = './assets/images/info';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['encrypt_name']         = TRUE; //enkripsi file name upload
        $config['overwrite']            = true;
        $config['max_size']             = 520; // 500kb
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('foto')) {
            $a = array('upload_data' => $this->upload->data());
            $gambar= $a['upload_data']['file_name'];
            $this->Info_akademik_model->tambah($gambar);
            $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
            redirect('admin/info_akademik');
        }else {
          $error = $this->upload->display_errors();
          $this->session->set_flashdata('message', '<div class="alert alert-danger role="alert">'.$error.'</div>');
          $this->session->set_flashdata('error', 'Data gagal ditambahkan!');
            redirect('admin/info_akademik/tambah');
        }
      }
    }
    public function ubah($id = null)
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['view'] = $this->Info_akademik_model->getById($id);
        $data['title'] = 'Ubah Info Akademik';
        $this->form_validation->set_rules('judul', 'Judul', 'required|trim', [
            'required' => 'Judul tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'required|trim', [
            'required' => 'deskripsi tidak boleh kosong!'
        ]);
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/info/ubah', $data);
            $this->load->view('templates/footer');
        } else {
					if (!empty($_FILES['foto']['name'])) {
						$config['upload_path']          = './assets/images/info';
						$config['allowed_types']        = 'gif|jpg|png|jpeg';
						$config['encrypt_name']         = TRUE; //enkripsi file name upload
						$config['overwrite']            = true;
						$config['max_size']             = 520; // 500kb
						$this->load->library('upload', $config);
						if ($this->upload->do_upload('foto')) {
								$a = array('upload_data' => $this->upload->data());
								$gambar= $a['upload_data']['file_name'];
								$this->Info_akademik_model->ubah($id,$gambar);
								$this->session->set_flashdata('success', 'Data berhasil diubah!');
								redirect('admin/info_akademik');
						}else {
							$error = $this->upload->display_errors();
							$this->session->set_flashdata('message', '<div class="alert alert-danger role="alert">'.$error.'</div>');
							$this->session->set_flashdata('error', 'Data gagal ditambahkan!');
							$getId = $this->input->post('id');
							redirect('admin/info_akademik/ubah/'.$getId.'/');
						}
					}else {
							$gambar = $this->input->post('foto_lama');
							$this->Info_akademik_model->ubah($id,$gambar);
							$this->session->set_flashdata('success', 'Data berhasil diubah!');
							redirect('admin/info_akademik');
					}
        }
    }

    public function hapus($id)
    {
        $this->Info_akademik_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('admin/info_akademik');
    }
		public function test()
    {
      $data = $this->Info_akademik_model->view();
      var_dump($data);
    }
}
