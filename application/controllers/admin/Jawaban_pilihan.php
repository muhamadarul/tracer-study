<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jawaban_pilihan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Pertanyaan_pilihan_model');
    $this->load->model('Jawaban_pilihan_model');

		$this->load->model('Auth_model');
		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('admin/auth');
		}
	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Jawaban Pilihan";
    $this->load->view('templates/header',$data);
    $this->load->view('admin/j_pilihan/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Jawaban_pilihan_model->view();
		echo json_encode($data);
	}
  public function getPertanyaan()
  {
    $data = $this->Pertanyaan_pilihan_model->view();
		echo json_encode($data);
  }
	public function tambah()
	{
		$ksp	 = $this->input->post('ksp');
    $jawaban = $this->input->post('jawaban');
		if ($ksp == '') {
		  $result['pesan'] ="Pertanyaan Harus DIpilih";
    }elseif ($jawaban == '') {
  	  $result['pesan'] ="Jawaban Harus Diisi";
		}else {
			$result['pesan'] ="";
			$data = [
			    'ksp' => $ksp,
          'jawaban' => $jawaban,
			 ];
			$this->Jawaban_pilihan_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Jawaban_pilihan_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_jksp');
    $ksp	 = $this->input->post('e_ksp');
    $jawaban = $this->input->post('e_jawaban');
    if ($ksp == '') {
      $result['pesan'] ="Pertanyaan Harus DIpilih";
    }elseif ($jawaban == '') {
      $result['pesan'] ="Jawaban Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'ksp' => $ksp,
          'jawaban' => $jawaban,
       ];
			$this->Jawaban_pilihan_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Jawaban_pilihan_model->hapus($id);
	}
}
