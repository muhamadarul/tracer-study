<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Email extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
		$this->load->model('Alumni_model');
		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('admin/auth');
		}
	}

	public function index()
	{
		$data['title'] = "Email Broadcast";
		$data['user'] = $this->Auth_model->success_login();
    $this->load->view('templates/header',$data);
    $this->load->view('admin/email/index');
    $this->load->view('templates/footer');
	}
  public function send()
  {
		$data = $this->Alumni_model->listemail();
		foreach ($data as $email => $key) {
			$this->load->library('PHPMailer_load'); //Load Library PHPMailer
	         $mail = $this->phpmailer_load->load(); // Mendefinisikan Variabel Mail
	         $mail->isSMTP();  // Mengirim menggunakan protokol SMTP
	         $mail->Host = 'smtp.gmail.com'; // Host dari server SMTP
	         $mail->SMTPAuth = true; // Autentikasi SMTP
	         $mail->Username = 'muhamadarul907@gmail.com';
	         $mail->Password = 'muhamadarul123';
	         $mail->SMTPSecure = 'tls';
	         $mail->Port = 587;
	         $mail->setFrom('noreply@tracerstudy-mic.com', 'Tracer Study'); // Sumber email
	         $mail->addAddress($key['email']); // Masukkan alamat email dari variabel $email
	         $mail->Subject = "Update Data Diri Anda Segera Di Aplikasi Tracer Study STMIK MIC"; // Subjek Email
	         $mail->msgHtml("Dear ".$key['nama'].",<br>Diberitahukan untuk para alumni STMIK MIC Cikarang, untuk segera memperbarui data diri Anda di Aplikasi Tracer Study STMIK MIC Cikarang <br>Login Disini <br> <strong><a href='' target='_blank'>tracerstudy.mic.ac.id</a></strong> <br>Hormat Kami<br><br><br>Tim Tracer Study.");
	         // Tampilkan pesan sukses atau error
					 $mail->send();
		}
		if ($mail->send() == true) {
			$this->session->flashdata('success','Email Berhasil Dikirim !');
		}else {
			$this->session->flashdata('error','Email Gagal Dikirim !');
			redirect('admin/email');
		}

  }

}
