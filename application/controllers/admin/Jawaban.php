<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jawaban extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Jawaban_model');
    $this->load->model('Kuesioner_model');
		$this->load->model('Auth_model');
		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('admin/auth');
		}
	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Jawaban";
    $this->load->view('templates/header',$data);
    $this->load->view('admin/jawaban/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Jawaban_model->view();
		echo json_encode($data);
	}
  public function getPertanyaan()
  {
    $data = $this->Kuesioner_model->view();
    echo json_encode($data);
  }
	public function tambah()
	{
    $kuesioner	 = $this->input->post('kuesioner');
    $jawaban = $this->input->post('jawaban');
    if ($kuesioner == '') {
      $result['pesan'] ="Kuesioner Harus DIpilih";
    }elseif ($jawaban == '') {
      $result['pesan'] ="Jawaban Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'kuesioner' => $kuesioner,
          'jawaban' => $jawaban,
       ];
			$this->Jawaban_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Jawaban_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_jawaban');
    $kuesioner	 = $this->input->post('e_kuesioner');
    $jawaban = $this->input->post('e_jawaban');
    if ($kuesioner == '') {
      $result['pesan'] ="Kuesioner Harus DIpilih";
    }elseif ($jawaban == '') {
      $result['pesan'] ="Jawaban Harus Diisi";
    }else {
      $result['pesan'] ="";
      $data = [
          'kuesioner' => $kuesioner,
          'jawaban' => $jawaban,
       ];
			$this->Jawaban_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Jawaban_model->hapus($id);
	}
}
