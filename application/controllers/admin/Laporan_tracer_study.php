<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_tracer_study extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
    $this->load->model('Jawaban_model');
    $this->load->model('Grafik_model');
    $this->load->model('Transaksi_kuesioner_model');
		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('admin/auth');
		}
	}

	public function index()
	{
		$data['title'] = "Laporan Tracer Study";
		$data['user'] = $this->Auth_model->success_login();
    $data['gaji'] = $this->Grafik_model->grafikPendapatan();
		$data['isi'] = $this->Grafik_model->grafikIsi();
		$data['aktivitas'] = $this->Grafik_model->grafikAktivitas();
		$data['kerjausaha'] = $this->Grafik_model->grafikKerjaUsaha();
		$data['kondisiKerja'] = $this->Grafik_model->grafikKondisiKerja();
		$data['korelasi'] = $this->Grafik_model->grafikKorelasi();
		$data['waktutunggu'] = $this->Grafik_model->grafikWaktuTunggu();
		$data['tingkatpendidikan'] = $this->Grafik_model->grafikTingkatPendidikan();
		$data['dapatkerja'] = $this->Grafik_model->grafikDapatKerja();
    $data['jawaban'] = $this->Jawaban_model->viewGaji();
    $this->load->view('templates/header',$data);
    $this->load->view('admin/tracer/index',$data);
    $this->load->view('templates/footer');
	}
}
