<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . 'third_party/spout-master/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Box\Spout\Common\Entity\Row;

class Alumni extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //load model
        $this->load->model('Alumni_model');
        $this->load->model('Auth_model');
        $this->load->model('Jurusan_model');
        $this->load->library('Excel');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('admin/auth');
        }
    }
    public function index()
    {
      $data['title'] = "Alumni";
      $data['user'] = $this->Auth_model->success_login();
      $this->load->view('templates/header',$data);
      $this->load->view('admin/alumni/index');
      $this->load->view('templates/footer');
    }
    public function ajax_list()
    {
        $list = $this->Alumni_model->get_datatables();
        $data = array();
        if(isset($_POST['start']) && isset($_POST['draw']))
        {
          $no = $_POST['start'];
        }else{
             // need start and draw
             die(); // some error
        }
        foreach ($list as $alumni) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $alumni->nama;
            $row[] = $alumni->tmp_lahir;
            $row[] = $alumni->tgl_lahir;
            $row[] = $alumni->email;
            if ($alumni->jurusan == 1) {
              $jurusan = "Sistem Informasi";
            }else {
              $jurusan = "Teknik Informatika";
            }
            $row[] = $jurusan;
            $row[] = $alumni->thn_lulus;
            //add html for action
            $row[] = '<a class="btn btn-default btn-outline-success btn-sm" href="javascript:void(0)" title="Edit" onclick="edit('."'".$alumni->id_alumni."'".')"><i class="fa fa-edit"></i></a>
                      <a class="btn btn-default btn-outline-warning btn-sm" href="javascript:void(0)" title="Detail" onclick="detail('."'".$alumni->id_alumni."'".')"><i class="fa fa-eye"></i></a>
                      <a class="btn btn-default btn-outline-danger btn-sm" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$alumni->id_alumni."'".')"><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Alumni_model->count_all(),
                        "recordsFiltered" => $this->Alumni_model->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    public function import()
    {
      $data['title'] = "Import Data Alumni";
      $data['jurusan'] = $this->Jurusan_model->view();
      $data['user'] = $this->Auth_model->success_login();
      $this->form_validation->set_rules('jurusan', 'Jurusan', 'required|trim', [
          'required' => 'Jurusan harus dipilih!'
      ]);
      if ($this->form_validation->run() == false) {
        $this->load->view('templates/header',$data);
        $this->load->view('admin/alumni/import');
        $this->load->view('templates/footer');
      } else {
        $jurusan = $this->input->post('jurusan');
        // $fileName = $_FILES['file']['name'];

          $config['upload_path'] = './assets/'; //path upload
          $config['encrypt_name'] = TRUE;
          // $config['file_name'] = $fileName;  // nama file
          $config['allowed_types'] = 'xls|xlsx|csv'; //tipe file yang diperbolehkan
          $config['max_size'] = 10000; // maksimal sizze

          $this->load->library('upload'); //meload librari upload
          $this->upload->initialize($config);

          if(! $this->upload->do_upload('file') ){
              echo $this->upload->display_errors();exit();
          }
          $a = array('upload_data' => $this->upload->data());
          $fileName = $a['upload_data']['file_name'];
          $inputFileName = './assets/'.$fileName;
          try {
                  $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                  $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                  $objPHPExcel = $objReader->load($inputFileName);
              } catch(Exception $e) {
                  die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
              }

              $sheet = $objPHPExcel->getSheet(0);
              $highestRow = $sheet->getHighestRow();
              $highestColumn = $sheet->getHighestColumn();

              for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
                  $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                  NULL,
                                                  TRUE,
                                                  FALSE);

                   // Sesuaikan key array dengan nama kolom di database
                   $data = array(
                      "nama"=> $rowData[0][0],
                      "tmp_lahir"=> $rowData[0][1],
                      "tgl_lahir"=> PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][2],'YYYY-MM-DD'),
                      "jk"=> $rowData[0][3],
                      "agama"=> $rowData[0][4],
                      "alamat"=> $rowData[0][5],
                      "email"=> $rowData[0][6],
                      "tlp"=> $rowData[0][7],
                      "password"=> password_hash($rowData[0][8], PASSWORD_DEFAULT),
                      "thn_lulus"=> $rowData[0][9],
                      "jurusan"=> $jurusan,
                  );

                  $insert = $this->db->insert("alumni",$data);
              }
              $this->session->set_flashdata('success', 'Data berhasil diimport!');
              redirect('admin/alumni/index');
      }
    }

    public function exportFileContoh()
    {
      $writer = WriterEntityFactory::createXLSXWriter();

          $writer->openToBrowser("format-import-alumni.xlsx");
          //silahkan sobat sesuaikan dengan data yang ingin sobat tampilkan
          $header = [
              WriterEntityFactory::createCell('Nama Mahasiswa'),
              WriterEntityFactory::createCell('Tempat Lahir'),
              WriterEntityFactory::createCell('Tgl Lahir/yyyy-mm-dd'),
              WriterEntityFactory::createCell('Jenis Kelamin'),
              WriterEntityFactory::createCell('Agama'),
              WriterEntityFactory::createCell('Alamat'),
              WriterEntityFactory::createCell('Email'),
              WriterEntityFactory::createCell('Telepon'),
              WriterEntityFactory::createCell('Password'),
              WriterEntityFactory::createCell('Tahun Lulus'),
          ];
          /** Tambah row satu kali untuk header */
          $singleRow = WriterEntityFactory::createRow($header);
          $writer->addRow($singleRow); //tambah row untuk header data
          $writer->close(); //tutup spout writer
    }
    private function _uploadImage()
    {
      //upload foto
      $config['upload_path']          = './assets/images/alumni';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';
      $config['encrypt_name'] = TRUE; //enkripsi file name upload
      $config['overwrite']            = true;
      $config['max_size']             = 520; // 500kb
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;
      $this->load->library('upload', $config);
      if ($this->upload->do_upload('foto')) {
          $a = array('upload_data' => $this->upload->data());
          $image= $a['upload_data']['file_name'];
          // $gambar = $_FILES['userfile']['name'];
      }else {
          $image= 'default.jpg';
          $foto = $this->upload->display_errors();
      }
    }
    public function tambah()
    {
      $nama = $this->input->post('nama');
      $tmp_lahir = $this->input->post('tmp_lahir');
      $tgl_lahir = $this->input->post('tgl_lahir');
      $agama = $this->input->post('agama');
      $alamat = $this->input->post('alamat');
      $email = $this->input->post('email');
      $tlp = $this->input->post('tlp');
      $jk  = $this->input->post('jk');
      $password  = $this->input->post('password');
      $thn_lulus  = $this->input->post('thn_lulus');
      $jurusan  = $this->input->post('jurusan');
      //upload foto
      $config['upload_path']          = './assets/images/alumni';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';
      $config['encrypt_name']         = TRUE; //enkripsi file name upload
      $config['overwrite']            = true;
      $config['max_size']             = 520; // 500kb
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;
      $this->load->library('upload', $config);
      if ($this->upload->do_upload('foto')) {
          $a = array('upload_data' => $this->upload->data());
          $image= $a['upload_data']['file_name'];
          // $gambar = $_FILES['userfile']['name'];
      }else {
          $image= 'default.jpg';
      }
      $error_upload = $this->upload->display_errors();

      if ($nama=='') {
        $result['pesan'] ="Nama Harus Diisi";
      }elseif ($tmp_lahir=='') {
        $result['pesan'] ="Tempat Lahir Harus Diisi";
      }elseif ($tgl_lahir=='') {
        $result['pesan'] ="Tanggal Lahir Harus Diisi";
      }elseif ($jk=='') {
        $result['pesan'] ="JK Harus Diisi";
      }elseif ($agama=='') {
        $result['pesan'] ="Agama Harus Dipilih";
      }elseif ($alamat=='') {
        $result['pesan'] ="Alamat Harus Diisi";
      }elseif ($email=='') {
        $result['pesan'] ="Email Harus Diisi";
      }elseif ($tlp=='') {
        $result['pesan'] ="Telepon Harus Diisi";
      }elseif ($password=='') {
        $result['pesan'] ="Password Harus Diisi";
      }elseif(strlen($password) < 5) {
        $result['pesan'] ="Password Minimal 5 Karakter";
      }elseif ($thn_lulus=='') {
        $result['pesan'] ="Tahun Lulus Harus Diisi";
      }elseif ($jurusan=='') {
        $result['pesan'] ="Jurusan Harus Diisi";
      }elseif($error_upload !='') {
        $result['pesan'] = $error_upload;
      }else {
        $result['pesan']="";
        $data = array(
                'nama' => $nama,
                'tmp_lahir' => $tmp_lahir,
                'tgl_lahir' => $tgl_lahir,
                'jk' => $jk,
                'agama' => $agama,
                'alamat' => $alamat,
                'email' => $email,
                'tlp' =>$tlp,
                'password' => password_hash($password, PASSWORD_DEFAULT),
                'thn_lulus' => $thn_lulus,
                'jurusan' => $jurusan,
                'foto' => $image,
              );
          $this->Alumni_model->tambah($data);
      }
      echo json_encode($result);
   }
   public function hapus()
   {
     $id = $this->input->post('id');
     $this->Alumni_model->hapus($id);
   }
   public function ajax_edit($id)
   {
     $data = $this->Alumni_model->getById($id);
     echo json_encode($data);
   }
   public function ubah()
   {
     $id = $this->input->post('id');
     $nama = $this->input->post('nama2');
     $tmp_lahir = $this->input->post('tmp_lahir2');
     $tgl_lahir = $this->input->post('tgl_lahir2');
     $agama = $this->input->post('agama2');
     $alamat = $this->input->post('alamat2');
     $email = $this->input->post('email2');
     $tlp = $this->input->post('tlp2');
     $jk  = $this->input->post('jk2');
     $password  = $this->input->post('password2');
     $thn_lulus  = $this->input->post('thn_lulus2');
     $jurusan  = $this->input->post('jurusan2');
     $image2  = $this->input->post('foto_lama');
     $alumni = $this->Alumni_model->getById($id);

     if ($nama=='') {
       $result['pesan'] ="Nama Harus Diisi";
     }elseif ($tmp_lahir=='') {
       $result['pesan'] ="Tempat Lahir Harus Diisi";
     }elseif ($tgl_lahir=='') {
       $result['pesan'] ="Tanggal Lahir Harus Diisi";
     }elseif ($jk=='') {
       $result['pesan'] ="JK Harus Diisi";
     }elseif ($agama=='') {
       $result['pesan'] ="Agama Harus Dipilih";
     }elseif ($alamat=='') {
       $result['pesan'] ="Alamat Harus Diisi";
     }elseif ($email=='') {
       $result['pesan'] ="Email Harus Diisi";
     }elseif ($tlp=='') {
       $result['pesan'] ="Telepon Harus Diisi";
     }elseif ($thn_lulus=='') {
       $result['pesan'] ="Tahun Lulus Harus Diisi";
     }elseif ($jurusan=='') {
       $result['pesan'] ="Jurusan Harus Diisi";
     }else {
       if ($password=='' && (empty($_FILES['foto2']['name']))) {
         $data = array(
                 'nama' => $nama,
                 'tmp_lahir' => $tmp_lahir,
                 'tgl_lahir' => $tgl_lahir,
                 'jk' => $jk,
                 'agama' => $agama,
                 'alamat' => $alamat,
                 'email' => $email,
                 'tlp' =>$tlp,
                 'thn_lulus' => $thn_lulus,
                 'jurusan' => $jurusan,
                 'foto' => $image2,
               );
           $result['pesan']="";
           $this->Alumni_model->ubah($data,$id);
       }elseif ($password !='' && (!empty($_FILES['foto2']['name']))) {
         //upload foto
         $config['upload_path']          = './assets/images/alumni';
         $config['allowed_types']        = 'gif|jpg|png|jpeg';
         $config['encrypt_name']         = TRUE; //enkripsi file name upload
         $config['overwrite']            = true;
         $config['max_size']             = 520; // 500kb
         // $config['max_width']            = 1024;
         // $config['max_height']           = 768;
         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto2')) {
             $a = array('upload_data' => $this->upload->data());
             $image= $a['upload_data']['file_name'];
             // $gambar = $_FILES['userfile']['name'];
         }else {
             $image= 'default.jpg';
         }
         $error_upload = $this->upload->display_errors();
         //jika upload gagal
           if($error_upload !='') {
           $result['pesan'] = $error_upload;
           //upload berhasil
           }else {
             $data = array(
                     'nama' => $nama,
                     'tmp_lahir' => $tmp_lahir,
                     'tgl_lahir' => $tgl_lahir,
                     'jk' => $jk,
                     'agama' => $agama,
                     'alamat' => $alamat,
                     'email' => $email,
                     'tlp' =>$tlp,
                     'thn_lulus' => $thn_lulus,
                     'jurusan' => $jurusan,
                     'foto' => $image,
                     'password' => password_hash($password, PASSWORD_DEFAULT),
                   );
             $result['pesan']="";
             $this->Alumni_model->ubah($data,$id);
           }
       }elseif ($password =='' && (!empty($_FILES['foto2']['name']))) {
         //upload foto
         $config['upload_path']          = './assets/images/alumni';
         $config['allowed_types']        = 'gif|jpg|png|jpeg';
         $config['encrypt_name']         = TRUE; //enkripsi file name upload
         $config['overwrite']            = true;
         $config['max_size']             = 520; // 500kb
         // $config['max_width']            = 1024;
         // $config['max_height']           = 768;
         $this->load->library('upload', $config);
         if ($this->upload->do_upload('foto2')) {
             $a = array('upload_data' => $this->upload->data());
             $image= $a['upload_data']['file_name'];
             // $gambar = $_FILES['userfile']['name'];
         }else {
             $image= 'default.jpg';
         }
         $error_upload = $this->upload->display_errors();
         //jika upload gagal
           if($error_upload !='') {
           $result['pesan'] = $error_upload;
           //upload berhasil
           }else {
             $data = array(
                     'nama' => $nama,
                     'tmp_lahir' => $tmp_lahir,
                     'tgl_lahir' => $tgl_lahir,
                     'jk' => $jk,
                     'agama' => $agama,
                     'alamat' => $alamat,
                     'email' => $email,
                     'tlp' =>$tlp,
                     'thn_lulus' => $thn_lulus,
                     'jurusan' => $jurusan,
                     'foto' => $image,
                   );
             $result['pesan']="";
             $this->Alumni_model->ubah($data,$id);
           }
       }else {
         $data = array(
                 'nama' => $nama,
                 'tmp_lahir' => $tmp_lahir,
                 'tgl_lahir' => $tgl_lahir,
                 'jk' => $jk,
                 'agama' => $agama,
                 'alamat' => $alamat,
                 'email' => $email,
                 'tlp' =>$tlp,
                 'thn_lulus' => $thn_lulus,
                 'jurusan' => $jurusan,
                 'foto' => $image2,
                 'password' => password_hash($password, PASSWORD_DEFAULT),
               );
           $result['pesan']="";
           $this->Alumni_model->ubah($data,$id);
       }
     }
     echo json_encode($result);
   }
   public function acakpassword()
   {
     $panjang = 10;
     $karakter= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
     $string = '';
     for ($i = 0; $i < $panjang; $i++) {
       $pos = rand(0, strlen($karakter)-1);
       $string .= $karakter{$pos};
    }
    return var_dump($string);
   }
   public function getJurusan()
   {
     $jurusan = $this->Jurusan_model->view();
     echo json_encode($jurusan);
   }
}
