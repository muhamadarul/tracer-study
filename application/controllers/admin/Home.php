<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;
// panggil restful api codeigniter
use CodeIgniter\RESTful\ResourceController;
class Home extends CI_Controller {

    public function index()
    {
        $this->load->helper('url');

        $this->load->view('example');
    }
}
