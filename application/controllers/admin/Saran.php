<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Saran extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('Saran_model');
		$this->load->model('Auth_model');
		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('admin/auth');
		}
	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Saran";
    $data['view'] = $this->Saran_model->view();
    $this->load->view('templates/header',$data);
    $this->load->view('admin/saran/index');
    $this->load->view('templates/footer');
	}
	public function cetak()
	{

	$saran = $this->Saran_model->cetakAll();
	$this->load->library('pdf');
	$this->pdf->set_option('isRemoteEnabled', TRUE);
	$this->pdf->setPaper('A4', 'landscape');
	$this->pdf->filename = "laporan-data-saran.pdf";
	$this->pdf->load_view('admin/saran/saran_pdf', compact('saran'));
	}
}
