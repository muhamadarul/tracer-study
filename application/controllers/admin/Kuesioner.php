<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kuesioner extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Kuesioner_model');
		$this->load->model('Auth_model');
		if (!$this->session->userdata('username')) {
						$this->session->set_flashdata('error', 'Anda belum melakukan login!');
						redirect('admin/auth');
		}
	}

	public function index()
	{
		$data['user'] = $this->Auth_model->success_login();
		$data['title'] = "Kuesioner";
    $this->load->view('templates/header',$data);
    $this->load->view('admin/kuesioner/index');
    $this->load->view('templates/footer');
	}
	public function view()
	{
		$data = $this->Kuesioner_model->view();
		echo json_encode($data);
	}
	public function tambah()
	{
		$kuesioner = $this->input->post('kuesioner');
		$urutan = $this->input->post('urutan');
		if ($kuesioner == '') {
		  $result['pesan'] ="Kuesioner Harus Diisi";
		}elseif ($urutan == "") {
			$result['pesan'] ="Urutan Harus Diisi";
		}else {
			$result['pesan'] ="";
			$data = [
			    'kuesioner' => $kuesioner,
					'urutan' => $urutan,

			 ];
			$this->Kuesioner_model->tambah($data);
		}
		echo json_encode($result);
	}
	public function getById()
	{
		$id = $this->input->post('id');
		$result = $this->Kuesioner_model->getById($id);
		echo json_encode($result);
	}
	public function ubah()
	{
		$id = $this->input->post('id_kuesioner');
		$kuesioner = $this->input->post('e_kuesioner');
		$urutan = $this->input->post('e_urutan');
		if ($kuesioner == '') {
		  $result['pesan'] ="Kuesioner Harus Diisi";
		}elseif ($urutan == "") {
			$result['pesan'] ="Urutan Harus Diisi";
		}else {
			$result['pesan'] ="";
			$data = [
			    'kuesioner' => $kuesioner,
					'urutan' => $urutan,

			 ];
			$this->Kuesioner_model->ubah($id,$data);
		}
		echo json_encode($result);
	}
	public function hapus()
	{
		$id = $this->input->post('id');
		$this->Kuesioner_model->hapus($id);
	}
}
