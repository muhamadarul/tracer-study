<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

	public function index()
	{
    $data['title'] = "FAQ";
		$this->load->view('templates/front_header',$data);
    $this->load->view('frontend/faq/index',$data);
    $this->load->view('templates/front_footer');
	}
}
