<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . '/libraries/REST_Controller.php';
require_once APPPATH . '/libraries/JWT.php';
use \Firebase\JWT\JWT;

class Auth extends REST_Controller
{
  public function __construct()
   {
       parent::__construct();
       $this->load->model('Alumni_model');
       $this->load->model('Auth_model');
   }
   public function privateKey()
    {
        $privateKey = "MIICXAIBAAKBgQC8kGa1pSjbSYZVebtTRBLxBz5H4i2p/llLCrEeQhta5kaQu/RnvNM";
        return $privateKey;
    }
   public function login_post()
   {
        $email      = $this->input->post('email');
        $password   = $this->input->post('password');
        $ceklogin  = $this->Auth_model->cek_login_alumni($email, $password);
        if(password_verify($password, $ceklogin['password']))
         {
             $secret_key = $this->privateKey();
             $issuer_claim = "THE_CLAIM"; // this can be the servername. Example: https://domain.com
             $audience_claim = "THE_AUDIENCE";
             $issuedat_claim = time(); // issued at
             $notbefore_claim = $issuedat_claim + 10; //not before in seconds
             $expire_claim = $issuedat_claim + 3600; // expire time in seconds
             $token = array(
                 "iss" => $issuer_claim,
                 "aud" => $audience_claim,
                 "iat" => $issuedat_claim,
                 "nbf" => $notbefore_claim,
                 "exp" => $expire_claim,
                 "data" => array(
                     "id_alumni" => $ceklogin['id_alumni'],
                     "email" => $ceklogin['email'],
                     "nama" => $ceklogin['nama'],
                 )
             );

             $token = JWT::encode($token, $secret_key);

             $output = [
                 'status' => 200,
                 'message' => 'Berhasil login',
                 "token" => $token,
                 "email" => $email,
                 "expireAt" => $expire_claim
             ];
             return $this->response($output, 200);
         } else {
             $output = [
                 'status' => 401,
                 'message' => 'Login failed',
                 "password" => $password
             ];
             return $this->response($output, 401);
         }
  }

}
